# Unofficial "Cats! la Mascarade" / "Cats! The Conspurracy" FoundryVTT System

![Banner Cats! la Mascarade](./system/assets/imgs/cats-banner.webp)

[![Buy Me a Coffee](./tags-bymecoffee.svg)](https://ko-fi.com/vlyan)
[![FoundryVTT version](https://img.shields.io/badge/FVTT-v12-informational)](https://foundryvtt.com/)
[![Forge Installs](https://img.shields.io/badge/dynamic/json?label=Forge%20Installs&query=package.installs&suffix=%25&url=https%3A%2F%2Fforge-vtt.com%2Fapi%2Fbazaar%2Fpackage%2Fcats-la-mascarade&colorB=4aa94a)](https://forge-vtt.com/bazaar#package=cats-la-mascarade)
[![Foundry Hub Endorsements](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fcats-la-mascarade%2Fshield%2Fendorsements)](https://www.foundryvtt-hub.com/package/cats-la-mascarade/)
[![Foundry Hub Comments](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fcats-la-mascarade%2Fshield%2Fcomments)](https://www.foundryvtt-hub.com/package/cats-la-mascarade/)

This is a fan-made game system for [Foundry Virtual Tabletop](https://foundryvtt.com/) provides a character sheet and basic systems to play "Cats! la Mascarade" / "Cats! The Conspurracy" published by [Black Book Edition](https://www.black-book-editions.fr/).

> As far as I know, this game is currently only in French, so all the translations are fan-made and may be inaccurate.
> <br>Update 2024-03-01 : This game will come in English with the title "Cats! The Conspurracy".

All texts, images and copyrights are the property of their respective owners.


## Installation
### With Search (recommended)
1. Open FoundryVTT.
2. In the `Game Systems` tab, click `Install system`.
3. Search `cats`, on the line `Cats! la Mascarade`, click `Install`.

### With the manifest
1. Open FoundryVTT.
2. In the `Game Systems` tab, click `Install system`.
3. Copy this link and use it in the `Manifest URL`, then click `Install`.
> https://gitlab.com/Vlyan/cats-la-mascarade/-/raw/master/system/system.json

### Non-English Compendiums
You will need the [Babele](https://foundryvtt.com/packages/babele) module to translate compendiums entries in another language.

1. Open FoundryVTT.
2. In the `Add-on Modules` tab, click `Install system`.
3. Search `babele`, then click `Install`.


## System helping (Contribute)
### Rules
You are free to contribute and propose corrections, modifications after fork. Try to respect these rules:
- Make sure you are up-to-date with the referent branch (most of the time the `dev` branch).
- Clear and precise commit messages allow a quick review of the code.
- If possible, limit yourself to one Feature per Merge request so as not to block the process.

### Dev install
1. Clone the repository.
2. Use `npm ci` to install the dependence.
3. Create a link from `<repo>/system` to your foundry system data (by default `%localappdata%/FoundryVTT/data/systems/cats-la-mascarade/`).

Windows example (modify the target and source directories, and run this in administrator) :
```bash
mklink /D /J "%localappdata%/FoundryVTT/data/systems/cats-la-mascarade" "D:/Projects/FVTT/cats-la-mascarade/system"
```

### Compiling SCSS
1. Run `npm run watch` to watch and compile the `scss` files.


## Screenshots
![Cats! FoundryVTT Connection](./screenshoots/cats-login.jpg?raw=true)
![Cats! FoundryVTT Pc and dice sheets](./screenshoots/cats-sheets.jpg?raw=true)
![Cats! FoundryVTT Items sheets](./screenshoots/cats-items-sheets.jpg?raw=true)
![Cats! FoundryVTT Compendiums](./screenshoots/cats-compendiums.jpg?raw=true)
