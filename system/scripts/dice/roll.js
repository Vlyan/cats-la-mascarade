/**
 * Cats! Roll
 */
export class CatsRoll extends Roll {
    static CHAT_TEMPLATE = "dice/chat-roll.html";
    static TOOLTIP_TEMPLATE = "dice/tooltip.html";
    static TOOLTIP_TEMPLATE_CRITICS = "dice/chat-roll-critics.html";

    /**
     * Sub Types of rolls
     */
    static types = {
        main: "main",
        successes: "successes",
        failures: "failures",
    };

    /**
     * Specific data added to the roll for template and others things
     */
    cats = {
        type:  CatsRoll.types.main,
        actor: null,
        actorInjuries: {
            value: 0,  // Actor injury level at the time of the roll
            max: 0,
        },
        target: null,
        item: null,
        flavor: null,
        critics: {
            successes: 0,
            failures: 0,
            failureMaxValue: 1,
        },
    };

    /**
     * Create a Roll
     *
     * Added Options :
     *   type            {CatsRoll.types} Type of the roll (main, successes, failures)
     *   actor           {CatsActor}      Any `Actor` object instance. Ex : `game.user.character`, `canvas.tokens.controlled[0].actor`.
     *   actorUuid       {string}         This is the `uuid` of an actor. Can be anything retrieved by `fromUuidSync()`.
     *   actorName       {string}         Careful this is case-sensitive. Ex : "Mra'ow".
     *   item            {CatsItem}       The item used for this roll.
     *   itemUuid        {string}         The item `uuid` used for this roll. Can be anything retrieved by `fromUuidSync()`.
     *   target          {TokenDocument}  The targeted Token.
     *   flavor          {string}         Text if no item (used for attributes)
     *   failureMaxValue {number}         Number of critic failure max value (1 to 6)
     */
    constructor(formula, data = {}, options = {}) {
        super(formula, data, options);

        // Type
        if (options.type && CatsRoll.types[options.type]) {
            this.cats.type = options.type;
        }

        // Actor
        [
            options?.actor,
            (options?.actorUuid ? fromUuidSync(options.actorUuid) : null),
            canvas.tokens?.controlled[0]?.actor,
            game.user.character,
        ].forEach((actor) => {
            if (!!actor && !this.cats.actor) {
                this.actor = actor;
            }
        });

        // Target Infos : get the 1st selected target
        const targetToken = Array.from(game.user.targets).values().next()?.value?.document;
        if (targetToken) {
            this.target = targetToken;
        }

        // Item (weapon/technique)
        if (options.item) {
            this.item = options.item;
        } else if (options.itemUuid) {
            this.item = fromUuidSync(options.itemUuid);
        }

        // Flavor
        if (options.flavor) {
            this.cats.flavor = options.flavor;
        }

        // FailureMaxValue
        if (options.failureMaxValue) {
            this.cats.critics.failureMaxValue = options.failureMaxValue;
        }
    }

    /**
     * Set actor
     * @param {CatsActor} actor
     */
    set actor(actor) {
        this.cats.actor = actor instanceof Actor ? actor : null;
        if (this.cats.actor) {
            this.cats.actorInjuries.value = this.cats.actor.system.gauges.injury_level.value;
            this.cats.actorInjuries.max   = this.cats.actor.system.gauges.injury_level.max;
        }
    }

    /**
     * Set Target
     * @param {TokenDocument} targetToken
     */
    set target(targetToken) {
        this.cats.target = targetToken || null;
    }

    /**
     * Set Item
     * @param {CatsItem} item
     */
    set item(item) {
        this.cats.item = item instanceof Item ? item : null;
    }

    /**
     * Execute the Roll, replacing dice and evaluating the total result
     * @override
     **/
    async evaluate({ minimize = false, maximize = false } = {}) {
        if (this._evaluated) {
            throw new Error(`The ${this.constructor.name} has already been evaluated and is now immutable`);
        }

        // Roll
        await super.evaluate({ minimize, maximize });

        return this;
    }

    /**
     * Render the tooltip HTML for a Roll instance
     * @return {Promise<string>}      The rendered HTML tooltip as a string
     * @override
     */
    getTooltip(contexte = null) {
        const chatData = {
            parts: this.dice.map(d => d.getTooltipData()),
            cats: this.cats,
        };

        // Alter main roll to add some css on dices
        if (this.cats.type === CatsRoll.types.main) {
            chatData.parts = chatData.parts.map(p => {
                p.rolls = p.rolls.map(r => {
                    const fail = r.result <= this.cats.critics.failureMaxValue;

                    if (r.classes.indexOf('discarded') < 1) {
                        // Add selected to the one kept
                        r.classes += " selected" + (fail ? " failure" : "");

                    } else if (fail) {
                        // Add failure css when "result <= failureMaxValue" (grayish otherwise)
                        r.classes = r.classes.replace("discarded", "failure");

                    } else if (r.classes.indexOf('discarded max') >= 0) {
                        // Un-grayish the max values
                        r.classes = r.classes.replace("discarded", "");
                    }
                    return r;
                });
                return p;
            });
        }

        return renderTemplate(
            CONFIG.cats.paths.templates + this.constructor.TOOLTIP_TEMPLATE,
            { ...chatData }
        );
    }

    /**
     * Modify actorInjuries on last 3 ranks to display icon
     * @param   {Object: {value: number, max: number}} actorInjuries
     * @returns {string}
     */
    static actorInjuryLabel(actorInjuries) {
        if (actorInjuries.max > 0) {
            if (actorInjuries.value === (actorInjuries.max - 2)) {
                return `<i class="fa-solid fa-bed text-red" title="${game.i18n.localize("cats.actor_sheet.injury_levels.unconsciousness")}"></i>`
            } else if (actorInjuries.value === (actorInjuries.max - 1)) {
                return `<i class="fa-solid fa-bed-pulse text-red" title="${game.i18n.localize("cats.actor_sheet.injury_levels.coma")}"></i>`
            } else if (actorInjuries.value === actorInjuries.max) {
                return `<i class="fa-solid fa-skull text-red" title="${game.i18n.localize("cats.actor_sheet.injury_levels.death")}"></i>`
            }
        }
        return actorInjuries.value;
    }

    /**
     * Render a Roll instance to HTML
     * @override
     */
    async render(chatOptions = {}) {
        chatOptions = foundry.utils.mergeObject(
            {
                user: game.user.id,
                template: CONFIG.cats.paths.templates + this.constructor.CHAT_TEMPLATE,
                // blind: false,
            },
            chatOptions
        );

        // Modify the template
        if ([CatsRoll.types.successes, CatsRoll.types.failures].includes(this.cats.type)) {
            chatOptions.template = CONFIG.cats.paths.templates + this.constructor.TOOLTIP_TEMPLATE_CRITICS;
        }

        // Execute the roll, if needed
        if (!this._evaluated) {
            await this.evaluate();
        }

        const isPrivate = chatOptions.isPrivate;
        const flavor    = isPrivate || !!this.cats.flavor ? null : chatOptions.flavor;
        const chatData  = {
            cats: this.cats,
            actorInjuriesText: CatsRoll.actorInjuryLabel(this.cats.actorInjuries),
            flavor,
            isPublicRoll: !isPrivate,
            formula: isPrivate ? "???" : this._formula,
            showProfilSection: !isPrivate && (this.cats.actor || this.cats.item || flavor),
            showDifficultyLevel: /^\dd10k[hl] \+/.test(this._formula),
            user: chatOptions.user,
            tooltip: isPrivate ? "" : await this.getTooltip({ from: "render" }),
            parts: this.dice.map(d => d.getTooltipData()),
            total: isPrivate ? "?" : Math.round(this.total * 100) / 100,
            noTargetDisclosure: false, // TODO tmp
        };

        // Render the roll display template
        return renderTemplate(chatOptions.template, chatData);
    }

    /**
     * Transform a Roll instance into a ChatMessage, displaying the roll result.
     * This function can either create the ChatMessage directly, or return the data object that will be used to create.
     * @override
     */
    async toMessage(messageData = {}, { rollMode = null, create = true } = {}) {
        // Perform the roll, if it has not yet been rolled
        if (!this._evaluated) {
            await this.evaluate();
        }

        // RollMode
        const rMode = rollMode || messageData.rollMode || game.settings.get("core", "rollMode");
        if (rMode) {
            messageData = ChatMessage.applyRollMode(messageData, rMode);
        }

        // Prepare chat data
        messageData = foundry.utils.mergeObject(
            {
                user: game.user.id,
                content: String(this.total),
                sound: CONFIG.sounds.dice,
                speaker: {
                    actor: this.cats.actor?.id || null,
                    token: this.cats.actor?.token || null,
                    alias: this.cats.actor?.name || null,
                },
            },
            messageData
        );
        messageData.rolls = [this];

        // Either create the message or just return the chat data
        return ChatMessage.implementation.create(messageData, {
            rollMode: rMode,
            temporary: !create,
        });
    }

    /** @override */
    static fromData(data) {
        const roll = super.fromData(data);

        roll.data = foundry.utils.duplicate(data.data);
        roll.cats = foundry.utils.duplicate(data.cats);

        // Get real Actor object
        if (data.cats.actor) {
            if (data.cats.actor instanceof Actor) {
                // Duplicate break the object, relink it
                roll.cats.actor = data.cats.actor;

            } else if (data.cats.actor.uuid) {
                // Only uuid, get the object
                let actor;
                const tmpItem = fromUuidSync(data.cats.actor.uuid);
                if (tmpItem instanceof Actor) {
                    actor = tmpItem;
                } else if (tmpItem instanceof TokenDocument) {
                    actor = tmpItem.actor;
                }
                if (actor) {
                    roll.cats.actor = actor;
                }
            }
        }

        // Get real Item object
        if (data.cats.item) {
            if (data.cats.item instanceof Item) {
                // Duplicate break the object, relink it
                roll.cats.item = data.cats.item;

            } else if (data.cats.item.uuid) {
                // Only uuid, get the object
                const tmpItem = fromUuidSync(data.cats.item.uuid);
                if (tmpItem) {
                    roll.cats.item = tmpItem;
                }
            }
        }

        // Get real Target object
        if (data.cats.target) {
            if (data.cats.target instanceof TokenDocument) {
                // Duplicate break the object, relink it
                roll.cats.target = data.cats.target;

            } else if (data.cats.target.uuid) {
                // Only uuid, get the object
                const tmpItem = fromUuidSync(data.cats.target.uuid);
                if (tmpItem) {
                    roll.cats.target = tmpItem;
                }
            }
        }

        return roll;
    }

    /**
     * Represent the data of the Roll as an object suitable for JSON serialization
     * @override
     */
    toJSON() {
        const json = super.toJSON();

        json.data = foundry.utils.duplicate(this.data);
        json.cats = foundry.utils.duplicate(this.cats);

        // Lightweight the Actor
        if (json.cats.actor && this.cats.actor?.uuid) {
            json.cats.actor = {
                uuid: this.cats.actor.uuid,
            };
        }

        // Lightweight the Item
        if (json.cats.item && this.cats.item?.uuid) {
            json.cats.item = {
                uuid: this.cats.item.uuid,
            };
        }

        // Lightweight the Target Token
        if (json.cats.target && this.cats.target?.uuid) {
            json.cats.target = {
                uuid: this.cats.target.uuid,
            };
        }

        return json;
    }
}
