/**
 * Cats! Dice picker dialog
 * @extends {FormApplication}
 */
export class CatsDicePickerDialog extends FormApplication {
    /**
     * Current Actor
     * @type {CatsActor}
     */
    _actor = null;

    /**
     * Current used Item (Skill, Weapon)
     * @type {CatsItem}
     */
    _item = null;

    /**
     * Current Target (Token)
     * @type {TokenDocument}
     */
    _target = null;

    /**
     * Chosen rule for injury altering critics
     * @type {"book"|"keep10"|"fixed"}
     */
    _chosenRule = game.settings.get(CONFIG.cats.moduleName, "chosenRuleInjuriesCritics");

    /**
     * Payload Object for variables in sheet
     */
    object = {
        attributeId: null,
        actionText: "", // for attributes
        luck: {
            baseDice: 0,
            addedDice: 0,
            worstDieSelected: false,
        },
        modifiers: {
            base: 0,
            userBonus: 0,
            userMalus: 0,
        },
    };

    /**
     * Assign the default options
     * @override
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "cats-dice-picker-dialog",
            classes: ["cats", "sheet", "sheet-dice-dialog", "dice-picker-dialog"],
            template: CONFIG.cats.paths.templates + "dice/dice-picker-dialog.html",
            title: game.i18n.localize("cats.dice.dice_picker.title"),
            width: 600,
            // height: 800,
            // resizable: false,
            closeOnSubmit: false,
            submitOnClose: false,
            submitOnChange: true,
        });
    }

    /**
     * Define a unique and dynamic element ID for the rendered application
     */
    get id() {
        return `cats-dice-picker-dialog-${this._actor?.id ?? "no-actor"}`;
    }

    /**
     * Create dialog
     *
     * ex: new game.cats.classes.CatsDicePickerDialog().render(true);
     *
     * Options :
     *   actor             {CatsActor}     Any `Actor` object instance. Ex : `game.user.character`, `canvas.tokens.controlled[0].actor`
     *   actorId           {string}        This is the `id` not the `uuid` of an actor. Ex : "AbYgKrNwWeAxa9jT"
     *   actorName         {string}        Careful this is case-sensitive. Ex : "Mra'ow"
     *   item              {CatsItem}      The object of skill or weapon used for this roll.
     *   itemUuid          {string}        The `uuid` of skill or weapon used for this roll. Can be anything retrieved by `fromUuidSync()`
     *   attributeId       {string}        The `id` of attribute used (tail, eye...)
     *   target            {TokenDocument} The targeted Token
     *   actionText        {string}        The action text (flavor) if no item / no attribute roll
     *   luckValue         {number}        1-5, needed if no actor provided
     *   userBonus         {number}        0-5
     *   userMalus         {number}        0-5
     *
     * @param options actor, actorId, actorName, item, itemUuid, attributeId, target, actionText, luckValue, userBonus, userMalus
     */
    constructor(options = {}) {
        super({}, options);

        // Luck value
        if (options.luckValue) {
            this.object.luck.baseDice = game.cats.helper.minMax(options.luckValue || 0);
        }

        // Try to get Actor from: options, first selected token or player's selected character
        [
            options?.actor,
            game.actors.get(options?.actorId),
            game.actors.getName(options?.actorName),
            canvas.tokens.controlled[0]?.actor,
            game.user.character,
        ].forEach((actor) => {
            if (actor && !this._actor) {
                this.actor = actor;
            }
        });

        // Target Infos
        if (options.target) {
            this.target = options.target;
        }
        if (!this._target) {
            // Get the 1st selected target
            const targetToken = Array.from(game.user.targets).values().next()?.value?.document;
            if (targetToken) {
                this.target = targetToken;
            }
        }

        // Item (weapon/technique)
        if (options.item) {
            this.item = options.item;
        } else if (options.itemUuid) {
            this.item = fromUuidSync(options.itemUuid);
        }

        // Others props
        this.attributeId = options.attributeId;

        if (options.actionText) {
            this.object.actionText = options.actionText;
        }

        // Bonus/Malus
        if (options.userBonus > 0) {
            this.object.modifiers.userBonus = game.cats.helper.minMax(options.userBonus);
        }
        if (options.userMalus > 0) {
            this.object.modifiers.userMalus = game.cats.helper.minMax(options.userMalus);
        }
    }

    /**
     * Refresh data (used from socket)
     */
    async refresh() {
        this.render(false);
    }

    /**
     * Set actor
     * @param {CatsActor} actor
     */
    set actor(actor) {
        if (!actor) {
            return;
        }
        if (!(actor instanceof Actor) || !actor.isOwner) {
            console.warn("Cats! | DP | Actor rejected : Not a valid Actor instance or permission was denied", actor);
            return;
        }

        this._actor = actor;

        if (this.object.luck.baseDice < 1 && actor?.system.attributes.luck > 0) {
            this.object.luck.baseDice = game.cats.helper.minMax(actor.system.attributes.luck);
        }
    }

    /**
     * Set used item
     * @param {CatsItem} item
     */
    set item(item) {
        if (!item) {
            return;
        }
        if (!(item instanceof Item) || !item.isOwner) {
            console.warn("Cats! | DP | Item rejected : Not a valid Item instance or permission was denied", item);
            return;
        }

        this._item = item;

        this.object.modifiers.base = item.system.rank + (item.system.base || 0);
    }

    /**
     * Set Target Infos object
     * @param {TokenDocument} targetToken
     */
    set target(targetToken) {
        if (!targetToken) {
            return;
        }
        if (!(targetToken instanceof TokenDocument)) {
            console.warn(
                "Cats! | DP | target rejected : Not a valid TokenDocument instance",
                targetToken
            );
            return;
        }
        this._target = targetToken;
    }

    /**
     * Set attributeId and his derivative
     * @param {string} id tail,eye...
     */
    set attributeId(id) {
        if (!id) {
            return;
        }
        if (!CONFIG.cats.attributes.includes(id)) {
            console.warn("Cats! | DP | attributeId rejected : Not a valid AttributeId", id);
            return;
        }

        this.object.attributeId = id;

        if (this._actor) {
            this.object.modifiers.base = (2 * this._actor.system.attributes[id]);
        }

        // Flavor text
        this.object.actionText = game.i18n.localize(`cats.actor_sheet.attributes.${id}`)
            + ` (${this.object.modifiers.base})`
        ;
    }

    /**
     * Add the Entity name into the window title
     * @type {String}
     */
    get title() {
        return game.i18n.localize("cats.dice.dice_picker.title") + (this._actor ? " - " + this._actor.name : "");
    }

    /**
     * Return the total modifiers for this roll
     */
    #getTotalRollModifier() {
        return this.object.modifiers.base
            + this.#getInjuryLevelRollModifier()
            + this.object.modifiers.userBonus
            - this.object.modifiers.userMalus
        ;
    }

    /**
     * Return the injury level modifier for this roll, for this actor (0, -1, ... ,-5)
     * @returns {number}
     */
    #getInjuryLevelRollModifier() {
        return this._actor?.injuryLevelRollModifier || 0;
    }

    /**
     * Return the critic failure max value for this actor (1 to 6)
     * @returns {number}
     */
    #getCriticFailureValue() {
        // Optional settings to ignore this rule
        if (this._chosenRule === "fixed") {
            return 1;
        }
        return Math.abs(this.#getInjuryLevelRollModifier()) + 1;
    }

    /**
     * Return true if the 10 can be a Lucky
     * @returns {boolean}
     */
    #isCriticInjuryDisplayOn10() {
        return this._chosenRule !== 'book' || this.#getCriticFailureValue() === 1;
    }

    /**
     * Construct and return the data object used to render the HTML template for this form application.
     * @override
     */
    async getData(options = null) {
        return {
            ...(await super.getData(options)),
            cssClass: this.options.classes.join(' '),
            unqIndex: this.id,
            actor: this._actor,
            item: this._item,
            target: this._target,
            totalRollModifier: this.#getTotalRollModifier(),
            criticFailureValue: this.#getCriticFailureValue(),
            injuryLevelRollModifier: this.#getInjuryLevelRollModifier(),
            injuryDisplay10: this.#isCriticInjuryDisplayOn10(),
        };
    }

    /**
     * Render the dialog
     * @override
     */
    render(force, options) {
        options = {
            ...options,
        };

        if (force === undefined) {
            force = true;
        }

        return super.render(force, options);
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param event    The initial triggering submission event
     * @param formData The object of validated form data with which to update the object
     * @returns        A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Update form values
        this.object.modifiers.userBonus   = Number(formData["modifiers.userBonus"] || 0);
        this.object.modifiers.userMalus   = Number(formData["modifiers.userMalus"] || 0);
        this.object.luck.addedDice        = Number(formData["luck.addedDice"] || 0);
        this.object.luck.worstDieSelected = !!formData["luck.worstDieSelected"];

        // Ignore auto submit after this point
        if (event.type !== 'submit') {
            return this.render(false);
        }


        // Luck value is needed
        if (this.object.luck.baseDice < 1) {
            ui.notifications.warn(game.i18n.localize("cats.dice.dice_picker.luck.value_needed"));
            return this.close();
        }

        // Build the formula
        const luckDice               = this.object.luck.baseDice + this.object.luck.addedDice;
        const chanceWorstDieSelected = this.object.luck.worstDieSelected;
        const totalRollModifier      = this.#getTotalRollModifier();
        const criticFailureValue     = this.#getCriticFailureValue();

        const formula = `${luckDice}d10k`
            + (chanceWorstDieSelected ? 'l' : 'h')
            + ` + ${totalRollModifier}`
        ;

        const rolls = {
            main: null,
            succ: null,
            fail: null,
        };

        // First roll, get the numbers of critics
        rolls.main = await Roll.create(formula, {}, {
            actor: this._actor,
            item: this._item,
            flavor: this.object.actionText,
            failureMaxValue: criticFailureValue,
        });

        await rolls.main.evaluate();

        const roll1Results = rolls.main.dice[0].results;
        const totalCritics = rolls.main.cats.critics;

        // Critical successes : only if no injuries on core rule, all for others choices
        if (this.#isCriticInjuryDisplayOn10()) {
            const criticalSuccess  = roll1Results.reduce((acc, r) => acc + (r.result === 10 ? 1 : 0), 0);
            if (criticalSuccess > 0) {
                rolls.succ = Roll.create(`${criticalSuccess}d10cs10`, {}, {
                    type: game.cats.classes.CatsRoll.types.successes,
                });
                await rolls.succ.evaluate();
                totalCritics.successes = rolls.succ.total;
            }
        }

        // Critical failures : can be 1-6
        const criticalFailures = roll1Results.reduce((acc, r) => acc + (r.result <= criticFailureValue ? 1 : 0), 0);
        if (criticalFailures > 0) {
            rolls.fail = await Roll.create(`${criticalFailures}d10cs1`, {}, {
                type: game.cats.classes.CatsRoll.types.failures,
                failureMaxValue: criticFailureValue,
            });
            await rolls.fail.evaluate();
            totalCritics.failures = rolls.fail.total;
        }


        // Update the actor
        if (this._actor && (totalCritics.successes + totalCritics.failures) > 0) {
            const exp = this._actor.system.gauges.experience;
            await this._actor.update({
                system: {
                    attributes: {
                        luck: game.cats.helper.minMax(
                            this._actor.system.attributes.luck
                            + (totalCritics.successes - totalCritics.failures)
                            , 1
                            , 5
                        )
                    },
                    gauges: {
                        experience: {
                            successes: exp.successes + totalCritics.successes,
                            failures: exp.failures + totalCritics.failures,
                            total: {
                                earn: exp.total.earn + totalCritics.successes + totalCritics.failures,
                            },
                        }
                    }
                }
            });
        }

        // Send to chat
        ChatMessage.create({
            flavor: null,
            user: game.user.id,
            rolls: Object.values(rolls).filter(r => !!r).map(r => r.toJSON()),
            rollMode: game.settings.get("core", "rollMode"),
            sound: CONFIG.sounds.dice,
            speaker: {
                actor: this._actor?.id || null,
                token: this._actor?.token || null,
                alias: this._actor?.name || null,
            },
        });

        return this.close();
    }

    /**
     * Subscribe to events from the sheet.
     * @param {jQuery} html HTML content of the sheet.
     */
    activateListeners(html) {
        super.activateListeners(html);

        // Commons
        game.cats.helper.commonListeners(html);
    }
}
