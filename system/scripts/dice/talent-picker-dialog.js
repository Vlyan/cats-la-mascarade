/**
 * Cats! Talent dialog
 * @extends {FormApplication}
 */
export class CatsTalentPickerDialog extends FormApplication {
    /**
     * Current Actor
     * @type {CatsActor}
     */
    _actor = null;

    /**
     * Current used Item (Skill, Weapon)
     * @type {CatsItem}
     */
    _item = null;

    /**
     * Current Target (Token)
     * @type {TokenDocument}
     * @private
     */
    _target = null;

    /**
     * Payload Object for variables in sheet
     */
    object = {
        rank: 0,
    };

    /**
     * Assign the default options
     * @override
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "cats-talent-picker-dialog",
            classes: ["cats", "sheet", "sheet-dice-dialog", "talent-picker-dialog"],
            template: CONFIG.cats.paths.templates + "dice/talent-picker-dialog.html",
            title: game.i18n.localize("cats.dice.talent_picker.title"),
            width: 600,
            // height: 800,
            // resizable: false,
            closeOnSubmit: false,
            submitOnClose: false,
            submitOnChange: true,
        });
    }

    /**
     * Define a unique and dynamic element ID for the rendered application
     */
    get id() {
        return `cats-talent-picker-dialog-${this._actor?.id ?? "no-actor"}`;
    }

    /**
     * Create dialog
     *
     * ex: new game.cats.classes.CatsDicePickerDialog().render(true);
     *
     * Options :
     *   actor             {CatsActor}     Any `Actor` object instance. Ex : `game.user.character`, `canvas.tokens.controlled[0].actor`
     *   actorId           {string}        This is the `id` not the `uuid` of an actor. Ex : "AbYgKrNwWeAxa9jT"
     *   actorName         {string}        Careful this is case-sensitive. Ex : "Mra'ow"
     *   item              {CatsItem}      The object of technique or weapon used for this roll.
     *   itemUuid          {string}        The `uuid` of technique or weapon used for this roll. Can be anything retrieved by `fromUuidSync()`
     *   target            {TokenDocument} The targeted Token
     *
     * @param options actor, actorId, actorName, item, itemUuid, target
     */
    constructor(options = {}) {
        super({}, options);

        // Try to get Actor from: options, first selected token or player's selected character
        [
            options?.actor,
            game.actors.get(options?.actorId),
            game.actors.getName(options?.actorName),
            canvas.tokens.controlled[0]?.actor,
            game.user.character,
        ].forEach((actor) => {
            if (actor && !this._actor) {
                this.actor = actor;
            }
        });

        // Target Infos
        if (options.target) {
            this.target = options.target;
        }
        if (!this._target) {
            // Get the 1st selected target
            const targetToken = Array.from(game.user.targets).values().next()?.value?.document;
            if (targetToken) {
                this.target = targetToken;
            }
        }

        // Item (weapon/technique)
        if (options.item) {
            this.item = options.item;
        } else if (options.itemUuid) {
            this.item = fromUuidSync(options.itemUuid);
        }
    }

    /**
     * Refresh data (used from socket)
     */
    async refresh() {
        this.render(false);
    }

    /**
     * Set actor
     * @param {CatsActor} actor
     */
    set actor(actor) {
        if (!actor) {
            return;
        }
        if (!(actor instanceof Actor) || !actor.isOwner) {
            console.warn("Cats! | TP | Actor rejected : Not a valid Actor instance or permission was denied", actor);
            return;
        }
        this._actor = actor;
    }

    /**
     * Set used item
     * @param {CatsItem} item
     */
    set item(item) {
        if (!item) {
            return;
        }
        if (!(item instanceof Item) || !item.isOwner) {
            console.warn("Cats! | TP | Item rejected : Not a valid Item instance or permission was denied", item);
            return;
        }

        this._item = item;

        this.object.rank = item.system.rank;
    }

    /**
     * Set Target Infos object
     * @param {TokenDocument} targetToken
     */
    set target(targetToken) {
        if (!targetToken) {
            return;
        }
        if (!(targetToken instanceof TokenDocument)) {
            console.warn(
                "Cats! | TP | target rejected : Not a valid TokenDocument instance",
                targetToken
            );
            return;
        }
        this._target = targetToken;
    }

    /**
     * Add the Entity name into the window title
     * @type {String}
     */
    get title() {
        return game.i18n.localize("cats.dice.talent_picker.title") + (this._actor ? " - " + this._actor.name : "");
    }

    /**
     * Construct and return the data object used to render the HTML template for this form application.
     * @override
     */
    async getData(options = null) {
        const gauges = this._actor.system.gauges;

        // Talent
        const talentValue = game.cats.helper.minMax(
            gauges.talent_levels_used.value + this.object.rank,
            gauges.talent_levels_used.value,
            gauges.talent_levels_used.max
        );

        // Injury
        const injuryLevelMaxValue = this._actor.getInjuryLevelMaxValue();
        const injuryValue = game.cats.helper.minMax(
            gauges.injury_level.value + game.cats.classes.CatsActor.getTalentModifierDiff(gauges.talent_levels_used.value, talentValue),
            gauges.injury_level.value,
            gauges.injury_level.max
        );

        return {
            ...(await super.getData(options)),
            cssClass: this.options.classes.join(' '),
            unqIndex: this.id,
            actor: this._actor,
            item: this._item,
            target: this._target,
            gauges: {
                talent: {
                    value: talentValue,
                    fatigueBarValues: game.cats.classes.CatsCharacterSheet.getTalentFatigueBarValues(talentValue),
                },
                injury: {
                    value: injuryValue,
                    config: {
                        numeric: injuryLevelMaxValue - 3,
                        unconscious: injuryLevelMaxValue - 2,
                        coma: injuryLevelMaxValue - 1,
                        death: injuryLevelMaxValue,
                    },
                },
            },
        };
    }

    /**
     * Render the dialog
     * @override
     */
    render(force, options) {
        options = {
            ...options,
        };

        if (force === undefined) {
            force = true;
        }

        return super.render(force, options);
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param event    The initial triggering submission event
     * @param formData The object of validated form data with which to update the object
     * @returns        A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Update form values
        this.object.rank = Number(formData["rank"] || 0);

        // Ignore auto submit after this point
        if (event.type !== 'submit') {
            return this.render(false);
        }
        if (!this._actor || this.object.rank < 1) {
            return this.close();
        }

        // Prepare actor data
        const gauges = this._actor.system.gauges;
        const talentValue = game.cats.helper.minMax(
            gauges.talent_levels_used.value + this.object.rank,
            0,
            40
        );
        // Need to compute the modifier for ChatMessage (this is done by the actor update but append to late)
        const injuryValue = game.cats.helper.minMax(
            gauges.injury_level.value + game.cats.classes.CatsActor.getTalentModifierDiff(gauges.talent_levels_used.value, talentValue),
            gauges.injury_level.value,
            gauges.injury_level.max
        );

        // Update the actor
        await this._actor.update({
            "system.gauges.talent_levels_used.value": talentValue,
        });

        // Create the template datas
        const actorInjuries = {
            value: injuryValue,
            max: gauges.injury_level.max,
        };
        const content = await renderTemplate(CONFIG.cats.paths.templates + "dice/talent-message.html", {
            rankUsed: this.object.rank,
            cats: {
                actor: this._actor,
                item: this._item,
                target: this._target,
                actorInjuries,
            },
            actorInjuriesText: game.cats.classes.CatsRoll.actorInjuryLabel(actorInjuries),
            showProfilSection: (this._actor || this._item),
            noTargetDisclosure: false, // TODO tmp
        });

        // Send to chat
        ChatMessage.create({
            content,
            flavor: null,
            user: game.user.id,
            type: CONST.CHAT_MESSAGE_STYLES.IC,
            rollMode: game.settings.get("core", "rollMode"),
            sound: CONFIG.sounds.notification, // dice,lock,notification,combat
            speaker: {
                actor: this._actor?.id || null,
                token: this._actor?.token || null,
                alias: this._actor?.name || null,
            },
        });

        return this.close();
    }

    /**
     * Subscribe to events from the sheet.
     * @param {jQuery} html HTML content of the sheet.
     */
    activateListeners(html) {
        super.activateListeners(html);

        // Commons
        game.cats.helper.commonListeners(html);
    }
}
