import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * Commun class for Advantages / Disadvantages types
 * @extends {ItemSheet}
 */
export class CatsAdvantageSheet extends CatsBaseItemSheet {
    /**
     * Sub Types of Advantage/Disadvantage
     */
    static types = ["advantage", "disadvantage"];

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "advantage"],
            template: CONFIG.cats.paths.templates + "items/advantage/advantage-sheet.html",
        });
    }

    /**
     * @return {Object|Promise}
     */
    async getData(options = {}) {
        const sheetData = await super.getData(options);

        // SubTypes : "advantage", "disadvantage"
        sheetData.data.subTypesList = CatsAdvantageSheet.types.map((e) => ({
            id: e,
            label: game.i18n.localize("cats.items_sheet." + e),
        }));

        return sheetData;
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param   {Event}  event    The initial triggering submission event
     * @param   {Object} formData The object of validated form data with which to update the object
     * @returns {Promise}         A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Change the image according to the type if this is already the case
        if (
            formData["system.advantage_type"] &&
            formData.img.startsWith(`${CONFIG.cats.paths.assets}icons/items/`)
        ) {
            formData.img = `${CONFIG.cats.paths.assets}icons/items/${
                formData["system.advantage_type"] === 'advantage' ? 'advantage' : 'disadvantage'
            }.svg`;
        }

        return super._updateObject(event, formData);
    }
}
