import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * @extends {ItemSheet}
 */
export class CatsAttributeSheet extends CatsBaseItemSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "attribute"],
            template: CONFIG.cats.paths.templates + "items/attribute/attribute-sheet.html",
        });
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param event {Event}       The initial triggering submission event
     * @param formData {Object}   The object of validated form data with which to update the object
     * @returns {Promise}         A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Make a "real" object
        formData = foundry.utils.expandObject(formData);

        // for all kinds
        formData.system.usable_by = 'CBH';

        return super._updateObject(event, formData);
    }
}
