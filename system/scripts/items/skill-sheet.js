import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * @extends {ItemSheet}
 */
export class CatsSkillSheet extends CatsBaseItemSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "skill"],
            template: CONFIG.cats.paths.templates + "items/skill/skill-sheet.html",
        });
    }

    /**
     * @override
     */
    async getData(options = {}) {
        const sheetData = await super.getData(options);

        // Translate formula to local
        sheetData.data.system.formula = CatsSkillSheet.#translateAttributesList(
            sheetData.data.system.formula,
            false
        );

        return sheetData;
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param   {Event}  event    The initial triggering submission event
     * @param   {Object} formData The object of validated form data with which to update the object
     * @returns {Promise}         A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Revert translation if needed
        formData["system.formula"] = CatsSkillSheet.#translateAttributesList(formData["system.formula"], true);

        // Sanitize the modifier's formula (C-1, H+1)
        if (formData["system.modifier"]) {
            formData["system.modifier"] = formData["system.modifier"].toUpperCase();

            // Remove invalids
            if (!/^[CBH][-+]\d+$/.test(formData["system.modifier"])) {
                formData["system.modifier"] = "";
            }
        }

        return super._updateObject(event, formData);
    }

    /**
     * Subscribe to events from the sheet.
     * @param {jQuery} html HTML content of the sheet.
     * @override
     */
    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable) {
            return;
        }

        // Autocomplete for formula
        game.cats.helper.autocomplete(
            html
            , "system.formula"
            , CONFIG.cats.attributes.map(id => game.i18n.localize(`cats.actor_sheet.attributes.${id}`))
            , " "
        );
    }

    /**
     * Translate formula attributes to local language
     * @param  {string}   formula
     * @param  {boolean}  textToId
     * @return {string}
     */
    static #translateAttributesList(formula, textToId) {
        // Translation map
        const map = Array.from(CONFIG.cats.attributes).reduce((acc, id) => {
            if (textToId) {
                acc[game.cats.helper.normalize(game.i18n.localize(`cats.actor_sheet.attributes.${id}`))] = id;

            } else {
                acc[id] = game.i18n.localize(`cats.actor_sheet.attributes.${id}`);
            }
            return acc;
        }, {});

        // Replacement
        const regExp = new RegExp("\\b(" + Object.keys(map).join("|") + ")\\b", "g");
        return formula.toLowerCase().replace(regExp, (match) => map[game.cats.helper.normalize(match)]);
    }
}
