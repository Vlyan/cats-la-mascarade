import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * Extend CatsBaseItemSheet with modifications for objects
 * @extends {ItemSheet}
 */
export class CatsItemSheet extends CatsBaseItemSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "item"],
            template: CONFIG.cats.paths.templates + "items/item/item-sheet.html",
        });
    }
}
