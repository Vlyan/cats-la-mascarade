import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * @extends {ItemSheet}
 */
export class CatsTalentSheet extends CatsBaseItemSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "talent"],
            template: CONFIG.cats.paths.templates + "items/talent/talent-sheet.html",
        });
    }
}
