import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * @extends {ItemSheet}
 */
export class CatsCatBreedSheet extends CatsBaseItemSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "cat-breed"],
            template: CONFIG.cats.paths.templates + "items/cat-breed/cat-breed-sheet.html",
            dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
        });
    }

    /**
     * @return {Object|Promise}
     */
    async getData(options = {}) {
        const sheetData = await super.getData(options);
        const system = sheetData.data.system;

        // Get full items
        sheetData.data.fullItemsList = await CatsCatBreedSheet.#getFullItemsList(system);

        return sheetData;
    }

    /**
     * Convert a lightweight object (id and name) to a Item
     * @param  {DataModel} system
     * @returns {Promise<{advantages: *, disadvantages: *}>}
     */
    static async #getFullItemsList(system) {
        const getItems = (async (adv) => {
            const item = await fromUuid(adv.uuid);
            if (item) {
                return item;
            }

            // Item not found
            console.warn(`Cats! | Unknown advantage uuid[${adv.uuid}], name[${adv.name}]`);
            return {
                img: `${CONFIG.cats.paths.assets}icons/items/${adv.advantage_type ? "advantage" : "disadvantage"}.svg`,
                id: adv.uuid, // to be removable
                uuid: adv.uuid,
                name: adv.name,
                advantage_type: adv.advantage_type,
                removed: true,
            };
        });

        return await Promise.all(system.advantages.map(getItems));
    }

    /**
     * Subscribe to events from the sheet.
     * @param {jQuery} html HTML content of the sheet.
     * @override
     */
    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable) {
            return;
        }

        // Delete a property
        html.find(`.item-delete`).on("click", this.#deleteAdvantage.bind(this));
    }

    /**
     * Handle dropped data on the Item sheet, only "advantage" allowed.
     */
    async _onDrop(event) {
        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable) {
            return;
        }

        const data = JSON.parse(event.dataTransfer?.getData("text/plain"));
        if (!data) {
            return;
        }

        const item = await fromUuid(data.uuid);
        if (!item || item.documentName !== "Item") {
            return;
        }

        // Final object has to be an advantage
        if (item.type !== "advantage") {
            return;
        }

        // Ok add item
        this.#addAdvantage(item);
    }

    /**
     * Add advantage to the current item
     * @param {CatsItem} item
     * @private
     */
    #addAdvantage(item) {
        let list = this.document.system.advantages;
        if (!Array.isArray(list)) {
            list = [];
        }

        // Skip if already exists
        if (list.findIndex((p) => p.uuid === item.uuid) !== -1) {
            return;
        }

        list.push({ uuid: item.uuid, name: item.name, advantage_type: item.system.advantage_type });

        this.document.update({ "system.advantages": list });
    }

    /**
     * Delete a advantage from the current item
     * @param {Event} event
     * @return {Promise<void>}
     * @private
     */
    #deleteAdvantage(event) {
        event.preventDefault();
        event.stopPropagation();

        let list = this.document.system.advantages;
        if (!Array.isArray(list)) {
            return;
        }

        const target   = $(event.currentTarget);
        const uuid     = target.data("item-uuid");
        const name     = target.siblings(".item-name").html();
        const tmpProps = list.find((p) => p.uuid === uuid);
        if (!tmpProps) {
            return;
        }

        const callback = async () => {
            list = list.filter((p) => p.uuid !== uuid);
            this.document.update({ "system.advantages": list });
        };

        // Holing Ctrl = without confirm
        if (event.ctrlKey) {
            return callback();
        }

        game.cats.helper.confirmDeleteDialog(
            game.i18n.format("cats.global.delete_confirm", { name: name ?? tmpProps.name }),
            callback
        );
    }
}
