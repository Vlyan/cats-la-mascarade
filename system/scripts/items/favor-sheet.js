import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * Commun class for Favor types
 * @extends {ItemSheet}
 */
export class CatsFavorSheet extends CatsBaseItemSheet {
    /**
     * Sub Types of Favors
     */
    static types = ["creditor", "indebted"];

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "favor"],
            template: CONFIG.cats.paths.templates + "items/favor/favor-sheet.html",
        });
    }

    /**
     * @return {Object|Promise}
     */
    async getData(options = {}) {
        const sheetData = await super.getData(options);

        // SubTypes : "indebted", "creditor"
        sheetData.data.subTypesList = CatsFavorSheet.types.map((e) => ({
            id: e,
            label: game.i18n.localize("cats.items_sheet." + e),
        }));

        return sheetData;
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param   {Event}  event    The initial triggering submission event
     * @param   {Object} formData The object of validated form data with which to update the object
     * @returns {Promise}         A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Change the image according to the type if this is already the case
        if (
            formData["system.favor_type"] &&
            formData.img.startsWith(`${CONFIG.cats.paths.assets}icons/items/`)
        ) {
            formData.img = `${CONFIG.cats.paths.assets}icons/items/${
                formData["system.favor_type"] === 'creditor' ? 'favor' : 'indebted'
            }.svg`;
        }

        return super._updateObject(event, formData);
    }
}
