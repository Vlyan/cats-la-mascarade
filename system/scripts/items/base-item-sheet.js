/**
 * Extend the basic ItemSheet with common modifications for all others items sheets
 * @extends {ItemSheet}
 */
export class CatsBaseItemSheet extends ItemSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "item"],
            //template: CONFIG.cats.paths.templates + "items/item/item-sheet.html",
            width: 600,
            height: 550,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }],
        });
    }

    /**
     * @return {Object|Promise}
     */
    async getData(options = {}) {
        const sheetData = await super.getData(options);

        sheetData.data.dtypes = ["String", "Number", "Boolean"];

        // Fix editable
        // sheetData.editable = this.isEditable;
        // sheetData.options.editable = sheetData.editable;

        // Editors enrichment
        sheetData.data.enrichedHtml = {
            description: await TextEditor.enrichHTML(sheetData.data.system.description, { async: true }),
        };

        return sheetData;
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param event {Event}       The initial triggering submission event
     * @param formData {Object}   The object of validated form data with which to update the object
     * @returns {Promise}         A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Make a "real" object
        formData = foundry.utils.expandObject(formData);

        // Convert back "Usable By" : checkboxes -> CBH
        if (formData.usableBy) {
            formData.system.usable_by =
                (formData.usableBy.cat      ? 'C' : '')
                + (formData.usableBy.bastet ? 'B' : '')
                + (formData.usableBy.human  ? 'H' : '');
        }

        return super._updateObject(event, formData);
    }

    /**
     * Subscribe to events from the sheet.
     * @param {jQuery} html HTML content of the sheet.
     * @override
     */
    activateListeners(html) {
        super.activateListeners(html);

        // Commons
        game.cats.helper.commonListeners(html);

        // Everything below here is only needed if the sheet is editable
        // if (!this.isEditable) {
        //     return;
        // }
    }
}
