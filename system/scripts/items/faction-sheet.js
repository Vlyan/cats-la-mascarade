import { CatsBaseItemSheet } from "./base-item-sheet.js";

/**
 * @extends {ItemSheet}
 */
export class CatsFactionSheet extends CatsBaseItemSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "sheet-item", "faction"],
            template: CONFIG.cats.paths.templates + "items/faction/faction-sheet.html",
        });
    }

}
