/**
 * Extends the Combat to process special things
 */
export class CatsCombat extends Combat {
    /**
     * Roll initiative for one or multiple Combatants within the Combat entity
     * @param {string|string[]} ids     A Combatant id or Array of ids for which to roll
     * @param {string|null} [formula]   A non-default initiative formula to roll. Otherwise, the system default is used.
     * @param {boolean} [updateTurn]    Update the Combat turn after adding new initiative scores to keep the turn on
     *                                  the same Combatant.
     * @param {object} [messageOptions] Additional options with which to customize created Chat Messages
     * @return {Promise<Combat>}        A promise which resolves to the updated Combat entity once updates are complete.
     */
    async rollInitiative(ids, { formula = null, updateTurn = true, messageOptions = {} } = {}) {
        if (!Array.isArray(ids)) {
            ids = [ids];
        }

        const updatedCombatants = [];
        for (const combatantId of ids) {
            const combatant = game.combat.combatants.find((c) => c.id === combatantId);
            if (!combatant || !combatant.actor || combatant.isDefeated) {
                continue;
            }

            const roll = new Roll("1d10");
            await roll.evaluate();

            // Tail > Luck > 1d10 (random)
            const atr = combatant.actor.system.attributes;
            const initiative = (atr.tail * 100) + (atr.luck * 10) + (+roll.result);

            updatedCombatants.push({
                _id: combatant.id,
                initiative: initiative,
            });
        }

        // Update all combatants at once
        await this.updateEmbeddedDocuments("Combatant", updatedCombatants);
        return this;

    }

    /**
     * Define how the array of Combatants is sorted in the displayed list of the tracker.
     * Cats! sort by : Tail > Luck > 1d10 (random)
     * @private
     */
    _sortCombatants(a, b) {
        const aAtr = a.actor.system.attributes;
        const bAtr = b.actor.system.attributes;

        if (aAtr.tail === bAtr.tail) {
            if (aAtr.luck === bAtr.luck) {
                if (a.initiative === b.initiative) {
                    return 0;
                }
                return b.initiative - a.initiative;
            }
            return bAtr.luck - aAtr.luck;
        }
        return bAtr.tail - aAtr.tail;
    }
}
