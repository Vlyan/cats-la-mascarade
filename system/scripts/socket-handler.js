/**
 * Socket Handler
 */
export class CatsSocketHandler {
    /**
     * Namespace in FVTT
     */
    static SOCKET_NAME = `system.cats-la-mascarade`;

    constructor() {
        this.registerSocketListeners();
    }

    /**
     * registers all the socket listeners
     */
    registerSocketListeners() {
        game.socket.on(CatsSocketHandler.SOCKET_NAME, (payload) => {
            switch (payload.type) {
                case "refreshAppId":
                    this._onRefreshAppId(payload);
                    break;

                case "openDicePicker":
                    this._onOpenDicePicker(payload);
                    break;

                default:
                    console.warn(new Error("Cats! | SH | This socket event is not supported"), payload);
                    break;
            }
        });
    }

    /**
     * Refresh an app by his "id", not "appId" (ex "cats-dialog-kZHczAFghMNYFRWe", not "65")
     *
     * Usage : game.cats.sockets.refreshAppId(appId);
     *
     * @param {String} appId
     */
    refreshAppId(appId) {
        game.cats.helper.debounce(appId, () => {
            game.socket.emit(CatsSocketHandler.SOCKET_NAME, {
                type: "refreshAppId",
                appId,
            });
        })();
    }
    _onRefreshAppId(payload) {
        const app = Object.values(ui.windows).find((e) => e.id === payload.appId);
        if (!app || typeof app.refresh !== "function") {
            return;
        }
        app.refresh();
    }

    /**
     * Remotely open the DicePicker
     *
     * Usage : game.cats.sockets.openDicePicker({
     *   users: game.users.players.filter(u => u.active && u.hasPlayerOwner),
     *   dpOptions: {
     *     attributeId: "pad",
     *     userBonus: 2,
     *   }
     * });
     *
     * @param {User[]}  users     Users list to trigger the DP (will be reduced to id for network perf.)
     * @param {Actor[]} actors    Actors list to trigger the DP (will be reduced to uuid for network perf.)
     * @param {Object}  dpOptions Any CatsDicePickerDialog.options
     */
    openDicePicker({ users = [], actors = [], dpOptions = {} }) {
        // At least one user or one actor
        if (foundry.utils.isEmpty(users) && foundry.utils.isEmpty(actors)) {
            console.error("Cats! | SH | openDicePicker - 'users' and 'actors' are both empty, use at least one.");
            return;
        }
        // Fail if dpOptions.actor* provided
        if (!foundry.utils.isEmpty(dpOptions?.actorName)) {
            console.error("Cats! | SH | openDicePicker - Do not use 'dpOptions.actorName', use 'actors' list instead.");
            return;
        }
        if (!foundry.utils.isEmpty(dpOptions?.actorId)) {
            console.error("Cats! | SH | openDicePicker - Do not use 'dpOptions.actorId', use 'actors' list instead.");
            return;
        }
        if (!foundry.utils.isEmpty(dpOptions?.actor)) {
            console.error("Cats! | SH | openDicePicker - Do not use 'dpOptions.actor', use 'actors' list instead.");
            return;
        }

        game.socket.emit(CatsSocketHandler.SOCKET_NAME, {
            type: "openDicePicker",
            users: users?.map((u) => u.id),
            actors: actors?.map((a) => a.uuid),
            dpOptions,
        });
    }
    _onOpenDicePicker(payload) {
        if (!foundry.utils.isEmpty(payload.users) && !payload.users.includes(game.user.id)) {
            return;
        }

        // Actors
        if (!foundry.utils.isEmpty(payload.actors)) {
            payload.actors.forEach((uuid) => {
                const actor = fromUuidSync(uuid);
                if (actor && actor.testUserPermission(game.user, "OWNER")) {
                    new game.cats.classes.CatsDicePickerDialog({
                        ...payload.dpOptions,
                        actor: actor,
                    }).render(true);
                }
            });
            return;
        }

        // User Only : Let the DP select the actor
        new game.cats.classes.CatsDicePickerDialog(payload.dpOptions).render(true);
    }
}
