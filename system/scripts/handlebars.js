/**
 * Custom Handlebars
 */
export const RegisterHandlebars = function () {
    /* ------------------------------------ */
    /* Localizations                        */
    /* ------------------------------------ */
    Handlebars.registerHelper("localizeSpecies", function (id) {
        const key = "cats.sheet.type." + id.toLowerCase();
        return game.cats.helper.sanitizeLocalize(key);
    });

    Handlebars.registerHelper("localizeYesNo", function (isYes) {
        return game.cats.helper.sanitizeLocalize(isYes ? "Yes" : "No");
    });

    // Transform "CBH" to "Cat, Bastet, Human"
    Handlebars.registerHelper("localizeUsableBy", function (trigram) {
        const text = [];
        const map = { C: 'cat', B: 'bastet', H: 'human' };

        trigram.split('').forEach(specie => {
            text.push(game.cats.helper.sanitizeLocalize(`cats.actor_sheet.species.${map[specie]}`));
        });
        return text.join(', ');
    });

    Handlebars.registerHelper("localizeDifficultyLevel", function (result) {
        let level = "";
        CONFIG.cats.difficulty.levels.every((limit) => {
            if (+limit <= result) {
                level = +limit
                return true;
            }
            return false;
        });
        return game.cats.helper.sanitizeLocalize(`cats.dice.difficulty.levels.${level}`);
    });


    /* ------------------------------------ */
    /* Utility                              */
    /* ------------------------------------ */
    // Json - Display an object in textarea (for debug)
    Handlebars.registerHelper("json", function (...objects) {
        objects.pop(); // remove this function call
        return new Handlebars.SafeString(objects.map((e) => `<textarea>${JSON.stringify(e)}</textarea>`));
    });

    // Loop x times + add. ex: {{#times data.system.gauges.nine_lives.max 0}}
    Handlebars.registerHelper('times', function(nb, add, block) {
        let accum = '';
        for(let i = 0; i < nb + add; ++i) {
            accum += block.fn(i);
        }
        return accum;
    });

    // Make the sum of values
    Handlebars.registerHelper('sum', function(...nb) {
        nb.pop(); // remove this function call
        return nb.reduce((acc, n) => acc + Number(n ?? 0), 0);
    });

    // Make the sum of values
    Handlebars.registerHelper('sanitizeFormula', function(...formula) {
        formula.pop(); // remove this function call
        return formula
            .join(' ')
            .trim()
            .replace(/\s*\+\s*([-+])\s*/, ' $1 ')
            .replace(/\s*[+-]\s*0\s*/, '');
    });

    // Make a UUID link
    // Handlebars.registerHelper('UUID', function(uuid, name) {
    //     return new Handlebars.SafeString(`<a class="content-link" draggable="true" data-uuid="${uuid}" data-link="">${name}</a>`);
    // });

    // Add props "checked" if a and b are equal ({{radioChecked a b}}
    // Handlebars.registerHelper("radioChecked", function (a, b) {
    //     return a === b ? new Handlebars.SafeString('checked="checked"') : "";
    // });

    // Concatenation
    // Handlebars.registerHelper("concat", function (...objects) {
    //     objects.pop(); // remove this function call
    //     return objects.join("");
    // });

    /**
     * Utility conditional, usable in nested expression
     * {{#ifCond (ifCond advancement.type '==' 'technique') '||' (ifCond item.system.technique_type '==' 'kata')}}
     * {{#ifCond '["distinction","passion"]' 'includes' item.system.peculiarity_type}}
     */
    Handlebars.registerHelper("ifCond", function (a, operator, b, options) {
        let result = false;
        switch (operator) {
            case "==":
                result = a == b;
                break;
            case "===":
                result = a === b;
                break;
            case "!=":
                result = a != b;
                break;
            case "!==":
                result = a !== b;
                break;
            case "<":
                result = +a < +b;
                break;
            case "<=":
                result = +a <= +b;
                break;
            case ">":
                result = +a > +b;
                break;
            case ">=":
                result = +a >= +b;
                break;
            case "&&":
                result = a && b;
                break;
            case "||":
                result = a || b;
                break;
            case "includes":
                result = a && b && a.includes(b);
                break;

            // *** Cats! specific ***
            case "usableBy":
                // CBH usableBy cat
                result = a && b && a.includes(b.slice(0,1).toUpperCase());
                break;
            default:
                break;
        }
        if (typeof options.fn === "function") {
            return result ? options.fn(this) : options.inverse(this);
        }
        return result;
    });
};
