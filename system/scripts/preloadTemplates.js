/**
 * All templates used into another (partials "{{>") need to be here
 * @returns {Promise<Function[]>}
 * @constructor
 */
export const PreloadTemplates = async function () {
    // Paths to "systems/cats-la-mascarade/templates/"
    const tpl = CONFIG.cats.paths.templates;
    return loadTemplates([
        // *** Actors : PC ***
        `${tpl}actors/character/attributes.html`,
        `${tpl}actors/character/experience.html`,
        `${tpl}actors/character/identity.html`,
        `${tpl}actors/character/inventory.html`,
        `${tpl}actors/character/narrative.html`,
        `${tpl}actors/character/skills.html`,
        `${tpl}actors/character/favors.html`,

        // *** Items ***
        `${tpl}items/item/item-entry.html`,
        `${tpl}items/item/item-infos.html`,
        `${tpl}items/item/item-usable-by.html`,
        `${tpl}items/skill/skill-entry.html`,
        `${tpl}items/advantage/advantage-entry.html`,
        `${tpl}items/talent/talent-entry.html`,
        `${tpl}items/favor/favor-entry.html`,

        // *** Dice ***
        `${tpl}dice/radio-toolbar.html`,
        `${tpl}dice/chat-profil.html`,
    ]);
};
