/**
 * Cats! Migration class
 */
export class CatsMigration {
    /**
     * Minimum Version needed for migration stuff to trigger
     * @type {string}
     */
    static NEEDED_VERSION = "1.12.0";

    /**
     * Return true if the version need some updates
     * @param {string} version Version number to contest against the current version
     * @return {boolean}
     */
    static needUpdate(version) {
        const currentVersion = game.settings.get(CONFIG.cats.moduleName, "systemMigrationVersion");
        return !currentVersion || foundry.utils.isNewerVersion(version, currentVersion);
    }

    /**
     * Perform a system migration for the entire World, applying migrations for Actors, Items, and Compendium packs
     * @param options
     * @return {Promise<void>} A Promise which resolves once the migration is completed
     */
    static async migrateWorld(options = { force: false }) {
        if (!game.user.isFirstGM) {
            return;
        }

        // if (CatsMigration.needUpdate("1.0.0")) {
        //     ChatMessage.create({"content": "<strong>Cats! v1.0.0 :</strong><br>"});
        // }

        // Warn the users
        ui.notifications.warn(
            `Applying Cats! System Migration for version ${game.system.version}.`
            + ` Please be patient and do not close your game or shut down your server.`,
            { permanent: true }
        );

        // Primary group for migration logs
        console.group(`Cats! | Migration for version ${game.system.version}`);

        const startGrpAndNotif = (str) => {
            ui.notifications.info(str, { permanent: false, console: false });
            console.groupCollapsed(str);
        }

        // *** World Actors ***
        startGrpAndNotif(`Migrate ${game.actors.size} World Actors`);
        for (let actor of game.actors.contents) {
            try {
                const updateData = await CatsMigration.#migrateActorData(actor, options);
                if (!foundry.utils.isEmpty(updateData)) {
                    console.log(`Migrating Actor document: ${actor.name}[${actor._id}]`);
                    await actor.update(updateData);
                }
            } catch (err) {
                err.message = `Failed migration for Actor ${actor.name}[${actor._id}]: ${err.message}`;
                console.error(err);
            }
        }
        console.groupEnd();
        // *** End of World Actors ***

        // *** World Items ***
        startGrpAndNotif(`Migrate ${game.items.size} World Items`);
        for (let item of game.items.contents) {
            try {
                const updateData = await CatsMigration.#migrateItemData(item, options);
                if (!foundry.utils.isEmpty(updateData)) {
                    console.log(`Migrating Item document: ${item.name}[${item._id}]`);
                    await item.update(updateData);
                }
            } catch (err) {
                err.message = `Failed migration for Item ${item.name}[${item._id}]: ${err.message}`;
                console.error(err);
            }
        }
        console.groupEnd();
        // *** End of World Items ***

        // *** Actor Override Tokens ***
        startGrpAndNotif(`Migrate ${game.scenes.size} World Scenes Actor's Tokens`);
        for (let scene of game.scenes.contents) {
            try {
                const updateData = CatsMigration.#migrateSceneData(scene, options);
                if (!foundry.utils.isEmpty(updateData)) {
                    console.log(`Migrating Scene documents: ${scene.name}[${scene._id}]`);
                    await scene.update(updateData);
                    // If we do not do this, then synthetic token actors remain in cache with the un-updated actorData.
                    scene.tokens.contents.forEach((t) => (t._actor = null));
                }
            } catch (err) {
                err.message = `Failed migration for Scene ${scene.name}[${scene._id}]: ${err.message}`;
                console.error(err);
            }
        }
        console.groupEnd();
        // *** End of Actor Override Tokens ***

        // *** World Compendium Packs ***
        startGrpAndNotif(`Migrate ${game.packs.size} World Compendium Packs`);
        for (let pack of game.packs) {
            if (pack.metadata.packageType !== "world" || !["Actor", "Item", "Scene"].includes(pack.metadata.type)) {
                continue;
            }
            await CatsMigration.#migrateCompendium(pack, options);
        }
        console.groupEnd();
        // *** End of World Compendium Packs ***

        // *** ChatMessages ***
        startGrpAndNotif(`Migrate ${game.collections.get("ChatMessage")?.size || 0} ChatMessages`);
        try {
            const updatedChatList = [];
            for (let message of game.collections.get("ChatMessage")) {
                const updateData = CatsMigration.#migrateChatMessage(message, options);
                if (!foundry.utils.isEmpty(updateData)) {
                    updateData["_id"] = message._id;
                    updatedChatList.push(updateData);
                }
            }
            // Save all the modified entries at once
            if (updatedChatList.length > 0) {
                console.log(`Migrating ${updatedChatList.length} ChatMessage documents`);
                await ChatMessage.updateDocuments(updatedChatList);
            }
        } catch (err) {
            err.message = "Failed migration for ChatMessage";
            console.error(err);
        }
        console.groupEnd();
        // *** End of ChatMessages ***


        // Close parent group
        console.groupEnd();

        // Set the migration as complete
        await game.settings.set(CONFIG.cats.moduleName, "systemMigrationVersion", game.system.version);

        ui.notifications.clear();
        ui.notifications.info(`Cats! System Migration to version ${game.system.version} completed!`, { permanent: true });
    }

    /**
     * Apply migration rules to all documents within a single Compendium pack
     * @param {CompendiumCollection} pack
     * @param options
     * @return {Promise}
     */
    static async #migrateCompendium(pack, options = { force: false }) {
        const docType = pack.metadata.type;
        const wasLocked = pack.locked;
        try {
            // Unlock the pack for editing
            await pack.configure({ locked: false });

            // Begin by requesting server-side data model migration and get the migrated content
            await pack.migrate();
            const documents = await pack.getDocuments();

            // Iterate over compendium entries - applying fine-tuned migration functions
            const updateDatasList = [];
            for (let doc of documents) {
                let updateData = {};

                switch (docType) {
                    case "Actor":
                        updateData = await CatsMigration.#migrateActorData(doc);
                        break;
                    case "Item":
                        updateData = await CatsMigration.#migrateItemData(doc);
                        break;
                    case "Scene":
                        updateData = CatsMigration.#migrateSceneData(doc);
                        break;
                }
                if (foundry.utils.isEmpty(updateData)) {
                    continue;
                }

                // Add the entry, if data was changed
                updateData["_id"] = doc._id;
                updateDatasList.push(updateData);

                console.log(`Migrating ${docType} document: ${doc.name}[${doc._id}] in Compendium ${pack.collection}`);
            }

            // Save the modified entries
            if (updateDatasList.length > 0) {
                await pack.documentClass.updateDocuments(updateDatasList, { pack: pack.collection });
            }
        } catch (err) {
            // Handle migration failures
            err.message = `Failed system migration for documents ${docType} in pack ${pack.collection}: ${err.message}`;
            console.error(err);
        }

        // Apply the original locked status for the pack
        await pack.configure({ locked: wasLocked });
        console.log(`Migrated all ${docType} contents from Compendium ${pack.collection}`);
    }

    /**
     * Migrate a single Scene document to incorporate changes to the data model of its actor data overrides
     * Return an Object of updateData to be applied
     * @param  {Scene} scene  The Scene data to Update
     * @param  options
     * @return {Object}       The updateData to apply
     */
    static #migrateSceneData(scene, options = { force: false }) {
        const tokens = Promise.all(scene.tokens.map(async (token) => {
            const t = token.toJSON();
            if (!t.actorId || t.actorLink) {
                t.delta = {};
            } else if (!game.actors.has(t.actorId)) {
                t.actorId = null;
                t.delta = {};
            } else if (!t.actorLink) {
                const actorData = foundry.utils.duplicate(t.delta);
                actorData.type = token.actor?.type;
                const update = await CatsMigration.#migrateActorData(actorData, options);
                ["items", "effects"].forEach((embeddedName) => {
                    if (!update[embeddedName]?.length) {
                        return;
                    }
                    const updates = new Map(update[embeddedName].map((u) => [u._id, u]));
                    t.delta[embeddedName].forEach((original) => {
                        const update = updates.get(original._id);
                        if (update) {
                            foundry.utils.mergeObject(original, update);
                        }
                    });
                    delete update[embeddedName];
                });

                foundry.utils.mergeObject(t.delta, update);
            }
            return t;
        }));
        return { tokens };
    }

    /**
     * Migrate a single Actor document to incorporate latest data model changes
     * Return an Object of updateData to be applied
     * @param  {Actor|Object} actor The actor, or the TokenDocument.delta to Update
     * @param  options
     * @return {Object} The updateData to apply
     */
    static async #migrateActorData(actor, options = { force: false }) {
        const updateData = {};
        const system = actor.system;

        // We need to be careful with unlinked tokens, only the diff is store in "actorData".
        // ex no diff : actor = {type: "npc"}, actorData = undefined
        if (!system) {
            return updateData;
        }

        // ***** Actor Items migration *****
        if (actor.items?.size) {
            console.groupCollapsed(`Migrating ${actor.items.size} Items for ${actor.name}`);
            for (const item of actor.items) {
                const updateData = await CatsMigration.#migrateItemData(item, options);
                if (!foundry.utils.isEmpty(updateData)) {
                    console.log(`Migrating ${item.type} document: ${item.name}[${item._id}]`);
                    await item.update(updateData);
                }
            }
            console.groupEnd();
        }
        // ***** End of Actor Items *****

        return updateData;
    }

    /**
     * Migrate a single Item document to incorporate latest data model changes
     * @param {CatsItem} item
     * @param options
     */
    static async #migrateItemData(item, options = { force: false }) {
        const updateData = {};

        // ***** Start of 1.12.0 *****
        if (options?.force || CatsMigration.needUpdate("1.12.0")) {
            let packItem;

            const packName = game.cats.helper.getCoreCompendiumNameByItemType(item);
            if (packName) {
                const itemName = game.cats.helper.normalize(item.name);
                const itemList = await game.packs.get(packName).getDocuments();
                packItem = itemList.find((e) =>
                    itemName === game.cats.helper.normalize(e.name)
                    || itemName === game.cats.helper.normalize(e.flags?.babele?.originalName)
                );
            }

            if (packItem?.uuid) {
                // Add source uuid to all items
                updateData["_stats.compendiumSource"] = packItem.uuid;

                // Add Modifier for Skills and Advantages
                switch (item.type) {
                    case "advantage": {
                        updateData["system.modifier.skill"]   = packItem.system.modifier?.skill || 0;
                        updateData["system.modifier.formula"] = packItem.system.modifier?.formula || "";
                        break;
                    }
                    case "skill": {
                        updateData["system.modifier"] = packItem.system.modifier || "";
                        break;
                    }
                }
            }
        }
        // ***** End of 1.12.0 *****

        return updateData;
    }

    /**
     * Migrate a single Item document to incorporate latest data model changes
     * @param {ChatMessage} message
     * @param options
     */
    static #migrateChatMessage(message, options = { force: false }) {
        // Nothing for now
        return {};
    }
}
