/**
 * GM Monitor Windows
 * @extends {FormApplication}
 */
export class CatsGmDuel extends FormApplication {
    /**
     * Settings
     */
    object = {
        aggressor: {
            actor: {},
            roll: 0,
            reputation: 0,
            targetDifficulty: 0,
        },
        defendant: {
            actor: {},
            roll: 0,
            reputation: 0,
            targetDifficulty: 0,
        },
    };

    /**
     * Assign the default options
     * @override
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "cats-gm-duel",
            classes: ["cats", "gm-duel"],
            template: CONFIG.cats.paths.templates + "gm/gm-duel.html",
            title: game.i18n.localize("cats.gm_duel.title"),
            width: 520,
            height: 170,
            resizable: true,
            closeOnSubmit: false,
            submitOnClose: false,
            submitOnChange: false,
            dragDrop: [{ dragSelector: null, dropSelector: null }],
        });
    }
    /**
     * Constructor
     * @param {ApplicationOptions} options
     */
    constructor(options = {}) {
        super(options);
        this.#addSelectedTokens();
    }

    /**
     * Add the Switch View button on top of sheet
     * @override
     */
    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();

        // Add selected tokens
        buttons.unshift({
            label: game.i18n.localize("cats.gm_monitor.add_selected_tokens"),
            class: "add-selected-token",
            icon: "fas fa-users",
            onclick: () =>
                game.cats.helper.debounce(
                    "AddSelectedToken-" + this.object.id,
                    () => this.#addSelectedTokens(),
                    1000,
                    true
                )(),
        });

        // Swap button
        buttons.unshift({
            label: game.i18n.localize("cats.gm_duel.swap_actors"),
            class: "swap-actors",
            icon: "fas fa-retweet",
            onclick: () =>
                game.cats.helper.debounce(
                    "SwapActors-" + this.object.id,
                    () => this.#swapAggressorDefender(),
                    1000,
                    true
                )(),
        });

        return buttons;
    }

    /**
     * Add selected token on duel
     */
    #addSelectedTokens() {
        if (canvas.tokens.controlled.length < 1) {
            return;
        }
        this.#addActor(canvas.tokens.controlled
            .map(t => t.actor)
            .filter(a => !!a)
        );
        this.render(false);
    }

    /**
     * Prevent non GM to render this windows
     * @override
     */
    render(force = false, options = {}) {
        if (!game.user.isGM) {
            return false;
        }
        return super.render(force, options);
    }

    /**
     * Construct and return the data object used to render the HTML template for this form application.
     * @param options
     * @return {Object}
     * @override
     */
    async getData(options = null) {
        return {
            ...(await super.getData(options)),
            cssClass: this.options.classes.join(' '),
            data: {
                ...this.object,
            },
        };
    }

    /**
     * Listen to html elements
     * @param {jQuery} html HTML content of the sheet.
     * @override
     */
    activateListeners(html) {
        super.activateListeners(html);

        if (!game.user.isGM) {
            return;
        }

        // Commons
        game.cats.helper.commonListeners(html);

        html.find(".open-user-dp").on("click", this.#openAttributeDiceRoll.bind(this, true));
        html.find(".ability-name").on("click", this.#openAttributeDiceRoll.bind(this, false));
        html.find(`.actor-remove-control`).on("click", this.#removeActor.bind(this));
    }

    /**
     * Handle dropped data on the Actor sheet
     * @param {DragEvent} event
     */
    async _onDrop(event) {
        // Need to be GM and editable
        if (!this.isEditable || !game.user.isGM) {
            return;
        }

        const json = event.dataTransfer.getData("text/plain");
        if (!json) {
            return;
        }

        const data = JSON.parse(json);
        if (!data || data.type !== "Actor" || !data.uuid) {
            return;
        }

        const actor = fromUuidSync(data.uuid);
        if (!actor) {
            return;
        }

        this.#addActor([actor]);

        return this.render(false);
    }

    /**
     * Remove the link to a property for the current item
     * @param {Event} event
     * @return {Promise<void>}
     * @private
     */
    async #removeActor(event) {
        event.preventDefault();
        event.stopPropagation();

        const uuid = $(event.currentTarget).data("actor-uuid");
        if (!uuid) {
            return;
        }

        if (this.object.aggressor.actor?.uuid === uuid) {
            this.#setAggressorDefender(this.object.defendant.actor);

        } else if (this.object.defendant.actor?.uuid === uuid) {
            this.#setAggressorDefender(this.object.aggressor.actor);
        }
        this.render(false);
    }

    /**
     * Add actor to duel sheet
     * @param {Actor[]} actors
     */
    #addActor(actors) {
        // No re-adding same actor
        actors = actors.filter(a => a.uuid !== this.object.aggressor.actor.uuid && a.uuid !== this.object.defendant.actor.uuid);

        const nbActors = actors.length;
        if (nbActors < 1) {
            return;
        }
        if (nbActors > 1) {
            // At least 2 tokens selected, set both at time
            this.#setAggressorDefender(actors[0], actors[1]);

        } else if (!foundry.utils.isEmpty(this.object.defendant.actor)) {
            // 3+ element : Cycle aggressor and defender places to pop the new actor at the defender place
            this.#setAggressorDefender(this.object.defendant.actor, actors[0]);

        } else if (!foundry.utils.isEmpty(this.object.aggressor.actor)) {
            // Second element : set to defender
            this.#setAggressorDefender(this.object.aggressor.actor, actors[0]);

        } else {
            // First element : set to aggressor
            this.#setAggressorDefender(actors[0]);
        }
    }

    /**
     * Set object's aggressor and defendant
     * @param {Actor} aggressorActor
     * @param {Actor} defendantActor
     */
    #setAggressorDefender(aggressorActor = {}, defendantActor = {}) {
        const rollValue = (attribute) => 2 * attribute;
        const targetValue = (attribute) => 5 + (2 * attribute);

        this.object = {
            aggressor: {
                actor: aggressorActor || {},
                roll: rollValue(aggressorActor?.system?.attributes?.pad || 0),
                reputation: this.#getReputationMaxAdvantage(aggressorActor),
                targetDifficulty: targetValue(defendantActor?.system?.attributes?.caress || 0)
            },
            defendant: {
                actor: defendantActor || {},
                roll: rollValue(defendantActor?.system?.attributes?.caress || 0),
                reputation: this.#getReputationMaxAdvantage(defendantActor),
                targetDifficulty: targetValue(aggressorActor?.system?.attributes?.pad || 0),
            }
        };
    }

    /**
     * Swap between Aggressor and Defender actor's
     */
    #swapAggressorDefender() {
        this.#setAggressorDefender(
            this.object.defendant.actor,
            this.object.aggressor.actor,
        );
        this.render(false);
    }

    /**
     * Open the dice-dialog for this attribute
     * @param {boolean} toNetwork If true send the DP to actor player screen (if logged)
     * @param {Event}   event
     * @private
     */
    #openAttributeDiceRoll(toNetwork, event) {
        event.preventDefault();
        event.stopPropagation();

        const uuid = $(event.currentTarget).data("actor-uuid");
        if (!uuid) {
            return;
        }

        const actor = fromUuidSync(uuid);
        if (!actor) {
            return;
        }

        const attributeId = $(event.currentTarget).data("id") || null;
        if (!attributeId || !actor.system.attributes[attributeId]) {
            console.warn("Cats! | Rolling attribute but attributeId is empty or unknown");
            return;
        }

        if (toNetwork && actor.hasPlayerOwnerActive) {
            // Open dice picker (socket)
            game.cats.sockets.openDicePicker({
                actors: [actor],
                attributeId,
                userBonus: this.#getReputationMaxAdvantage(actor),
            });
            ui.notifications.info(game.i18n.localize("cats.gm_duel.send_dp_confirm"));

        } else {
            // Open dice picker (local)
            new game.cats.classes.CatsDicePickerDialog({
                actor,
                attributeId,
                userBonus: this.#getReputationMaxAdvantage(actor),
            }).render(true);
        }
    }

    /**
     * Return the maximum bonus from the actor reputation (1pt every 5 rep.)
     * @param {Actor} actor
     */
    #getReputationMaxAdvantage(actor) {
        if (!actor?.system?.identity?.reputation) {
            return 0;
        }
        return Math.floor(actor.system.identity.reputation / 5);
    };
}
