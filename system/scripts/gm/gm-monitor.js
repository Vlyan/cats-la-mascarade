/**
 * GM Monitor Windows
 * @extends {FormApplication}
 */
export class CatsGmMonitor extends FormApplication {
    /**
     * Settings
     */
    object = {
        actors: [],
    };

    /**
     * Assign the default options
     * @override
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: "cats-gm-monitor",
            classes: ["cats", "gm-monitor"],
            template: CONFIG.cats.paths.templates + "gm/gm-monitor.html",
            title: game.i18n.localize("cats.gm_monitor.title"),
            width: 800,
            height: 300,
            resizable: true,
            closeOnSubmit: false,
            submitOnClose: false,
            submitOnChange: false,
            dragDrop: [{ dragSelector: null, dropSelector: null }],
        });
    }

    /**
     * Add the Switch View button on top of sheet
     * @override
     */
    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();

        // Add selected tokens
        buttons.unshift({
            label: game.i18n.localize("cats.gm_monitor.add_selected_tokens"),
            class: "add-selected-token",
            icon: "fas fa-users",
            onclick: () =>
                game.cats.helper.debounce(
                    "AddSelectedToken-" + this.object.id,
                    () => this.#addSelectedTokens(),
                    1000,
                    true
                )(),
        });

        // Gm Duel
        buttons.unshift({
            label: game.i18n.localize("cats.gm_duel.title"),
            class: "gm-duel",
            icon: "fas fa-shield-cat",
            onclick: () =>
                game.cats.helper.debounce(
                    "GmDuel-" + this.object.id,
                    () => (new game.cats.classes.CatsGmDuel()).render(true),
                    1000,
                    true
                )(),
        });
        return buttons;
    }

    /**
     * Constructor
     * @param {ApplicationOptions} options
     */
    constructor(options = {}) {
        super(options);
        this.#initialize();
    }

    /**
     * Refresh data (used from socket)
     */
    async refresh() {
        if (!game.user.isGM) {
            return;
        }
        this.#initialize();
        this.render(false);
    }

    /**
     * Initialize the values
     * @private
     */
    #initialize() {
        let actors;
        const uuidList = game.settings.get(CONFIG.cats.moduleName, "gmMonitorActorsUuids");

        if (uuidList.length > 0) {
            // Get actors from stored uuids
            actors = uuidList
                .map(uuid => {
                    const doc = fromUuidSync(uuid);
                    if (doc instanceof TokenDocument) {
                        return doc.actor;
                    }
                    return doc;
                })
                .filter(a => !!a); // skip null

        } else {
            // If empty add all pcs with owner
            actors = game.actors.filter((actor) => actor.type === "character" && actor.hasPlayerOwnerActive);
            this._saveActorsIds();
        }

        // Sort by name asc
        actors.sort((a, b) => {
            return a.name.localeCompare(b.name);
        });

        this.object.actors = actors;
    }

    /**
     * Add selected token on monitor if not already present
     */
    #addSelectedTokens() {
        if (canvas.tokens.controlled.length > 0) {
            const actors2Add = canvas.tokens.controlled
                .map(t => t.actor)
                .filter(t => !!t && !this.object.actors.find((a) => a.uuid === t.uuid));

            if (actors2Add.length < 1) {
                return;
            }

            this.object.actors = [
                ...this.object.actors,
                ...actors2Add
            ];
            this._saveActorsIds().then(() => this.render(false));
        }
    }

    /**
     * Prevent non GM to render this windows
     * @override
     */
    render(force = false, options = {}) {
        if (!game.user.isGM) {
            return false;
        }
        return super.render(force, options);
    }

    /**
     * Construct and return the data object used to render the HTML template for this form application.
     * @param options
     * @return {Object}
     * @override
     */
    async getData(options = null) {
        // Add split items section to actors
        this.object.actors.forEach(actor => {
            const splitItems = {
                talent: [],
                advantage: [],
                faction: {},
                cat_breed: {},
            };

            actor.items.forEach((item) => {
                if (["talent", "advantage"].includes(item.type)) {
                    splitItems[item.type].push(item);
                } else if (["faction", "cat_breed"].includes(item.type)) {
                    splitItems[item.type] = item;
                 }
            });

            // Sort Items by name
            Object.values(splitItems).forEach(section => {
                if (Array.isArray(section)) {
                    section.sort((a, b) => a.name.localeCompare(b.name));
                }
            });

            actor.splitItems = splitItems;
        });

        return {
            ...(await super.getData(options)),
            cssClass: this.options.classes.join(' '),
            data: {
                actors: this.object.actors,
            },
        };
    }

    /**
     * Listen to html elements
     * @param {jQuery} html HTML content of the sheet.
     * @override
     */
    activateListeners(html) {
        super.activateListeners(html);

        if (!game.user.isGM) {
            return;
        }

        // Commons
        game.cats.helper.commonListeners(html);

        // Delete
        html.find(`.actor-remove-control`).on("click", this._removeActor.bind(this));

        // Add/Subtract
        html.find(`.actor-modify-control`).on("mousedown", this._modifyActor.bind(this));
    }

    /**
     * Handle dropped data on the Actor sheet
     * @param {DragEvent} event
     */
    async _onDrop(event) {
        // Need to be GM and editable
        if (!this.isEditable || !game.user.isGM) {
            return;
        }

        const json = event.dataTransfer.getData("text/plain");
        if (!json) {
            return;
        }

        const data = JSON.parse(json);
        if (!data || data.type !== "Actor" || !data.uuid || !!this.object.actors.find((a) => a.uuid === data.uuid)) {
            return;
        }

        const actor = fromUuidSync(data.uuid);
        if (!actor) {
            return;
        }

        this.object.actors.push(actor);

        return this._saveActorsIds();
    }

    /**
     * Save the actors ids in settings
     * @return {Promise<*>}
     * @private
     */
    async _saveActorsIds() {
        return game.settings.set(
            CONFIG.cats.moduleName,
            "gmMonitorActorsUuids",
            this.object.actors.map((a) => a.uuid)
        );
    }

    /**
     * Remove the link to a property for the current item
     * @param {Event} event
     * @return {Promise<void>}
     * @private
     */
    async _removeActor(event) {
        event.preventDefault();
        event.stopPropagation();

        const uuid = $(event.currentTarget).data("actor-uuid");
        if (!uuid) {
            return;
        }

        this.object.actors = this.object.actors.filter((e) => e.uuid !== uuid);

        return this._saveActorsIds();
    }

    /**
     * Add or subtract lives/injury/talent
     * @param event
     * @return {Promise<void>}
     * @private
     */
    async _modifyActor(event) {
        event.preventDefault();
        event.stopPropagation();

        const type = $(event.currentTarget).data("type")
        const uuid = $(event.currentTarget).data("actor-uuid");
        if (!uuid) {
            console.warn("Cats! | actor id not set", uuid);
            return;
        }

        let actor = fromUuidSync(uuid);
        if (!actor) {
            console.warn("Cats! | Actor not found", uuid);
            return;
        }

        // Get token's actor
        if (actor instanceof TokenDocument) {
            actor = actor?.actor;
        }

        // Mouse bt : middle = 0, left +1, right -1
        const middle = event.which === 2;
        const mod    = middle ? -99 : event.which === 1 ? 1 : -1;
        const gauges = actor.system.gauges;

        switch (type) {
            case "nine_lives":
                if (middle) {
                    ui.notifications.warn(game.i18n.localize("cats.gm_monitor.no_middle_mouse"));
                } else {
                    await actor.update({
                        "system.gauges.nine_lives.value" : game.cats.helper.minMax(
                            gauges.nine_lives.value + mod,
                            0,
                            gauges.nine_lives.max
                        ),
                    });
                }
                break;

            case "injury_level":
                await actor.update({
                    "system.gauges.injury_level.value" : game.cats.helper.minMax(
                        gauges.injury_level.value + mod,
                        0,
                        gauges.injury_level.max
                    ),
                });
                break;

            case "talent_levels":
                await actor.update({
                    "system.gauges.talent_levels_used.value" : game.cats.helper.minMax(
                        gauges.talent_levels_used.value + mod,
                        0,
                        gauges.talent_levels_used.max
                    )
                });
                break;

            case "reputation":
                if (middle) {
                    ui.notifications.warn(game.i18n.localize("cats.gm_monitor.no_middle_mouse"));
                } else {
                    await actor.update({
                        "system.identity.reputation": game.cats.helper.minMax(
                            actor.system.identity.reputation + mod,
                            0,
                            30
                        )
                    });
                }
                break;

            default:
                console.warn("Cats | Unsupported type", type);
                break;
        }

        this.render(false);
    }
}
