/**
 * Cats Actor Sheet
 */
export class CatsCharacterSheet extends ActorSheet {
    /**
     * Commons options
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ["cats", "sheet", "character"],
            template: CONFIG.cats.paths.templates + "actors/character-sheet.html",
            width: 640,
            height: 800,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "skills" }],
            dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
        });
    }

    /**
     * Return a light sheet if in "limited" state
     * @override
     */
    get template() {
        if (!game.user.isGM && this.actor.limited) {
            return `${CONFIG.cats.paths.templates}actors/character-sheet-limited.html`;
        }
        return this.options.template;
    }

    /** @inheritdoc */
    async getData(options = {}) {
        const sheetData = await super.getData(options);
        const system = sheetData.data.system;

        // GM settings
        sheetData.data.showAllSkills = !game.settings.get(CONFIG.cats.moduleName, "removeUnusableSkills");

        // Editors enrichment
        sheetData.data.enrichedHtml = {
            description: await TextEditor.enrichHTML(system.description, { async: true }),
            notes: await TextEditor.enrichHTML(system.notes, { async: true }),
            experienceLogs: await TextEditor.enrichHTML(system.gauges.experience.logs, { async: true }),
        };

        // Shortcut for some tests
        sheetData.data.editable_not_soft_locked = sheetData.editable && !system.soft_locked;
        sheetData.data.isGM = game.user.isGM;

        // "Type" in sheet
        sheetData.data.speciesList   = Object.keys(CONFIG.cats.species).map(id => ({id, label: game.i18n.localize(`cats.actor_sheet.species.${id}`)}));
        sheetData.data.speciesConfig = CONFIG.cats.species[system.specie];

        // Derived
        sheetData.data.talentFatigueBarValues = CatsCharacterSheet.getTalentFatigueBarValues(system.gauges.talent_levels_used.value);

        const injuryLevelMaxValue = this.object.getInjuryLevelMaxValue();
        sheetData.data.injuryLevelMaxConfig = {
            numeric: injuryLevelMaxValue - 3,
            unconscious: injuryLevelMaxValue - 2,
            coma: injuryLevelMaxValue - 1,
            death: injuryLevelMaxValue,
        };

        // Split Items by types
        sheetData.data.splitItemsList = this.#splitItems();

        return sheetData;
    }

    /**
     * Split Items by types for better readability
     * @private
     */
    #splitItems() {
        const out = {
            skill: [],
            talent: [],
            advantage: [],
            disadvantage: [],
            item: [],
            indebted: [],
            creditor: [],
            faction: {},
            cat_breed: {},
        };

        // Get from actor to have real object and their uuid
        this.actor.items.forEach((item) => {
            switch (item.type) {
                case "item":  // no break
                case "skill": // no break
                case "talent":
                    out[item.type].push(item);
                    break;

                case "advantage":
                    // Split by subtypes
                    out[item.system.advantage_type].push(item);
                    break;

                case "favor":
                    // Split by subtypes
                    out[item.system.favor_type].push(item);
                    break;

                case "faction":
                case "cat_breed":
                    // Only one item
                    out[item.type] = item;
                    break;
            }
        });

        // Sort Items by name
        Object.values(out).forEach(section => {
            if (Array.isArray(section)) {
                section.sort((a, b) => a.name.localeCompare(b.name));
            }
        });

        return out;
    }

    /**
     * Return the label for this talent fatigue level
     * ex : "[null,0,0,0,0]", "[-1,-2,-2,-3,-4]"...
     * @param   {number} level Current level of fatigue
     * @param   {number} nb    Elements to display above and after
     * @returns {array}
     */
    static getTalentFatigueBarValues(level, nb = 5) {
        const levels = [];
        for (let i = (level-nb); i <= (level+nb); i++) {
            levels.push(game.cats.classes.CatsActor.getTalentModifier(i));
        }
        return levels;
    }

    /**
     * Handle dropped data on the Actor sheet.
     */
    async _onDrop(event) {
        // *** Everything below here is only needed if the sheet is editable and not locked ***
        if (!this.isEditable || this.actor.system.soft_locked) {
            console.log("Cats! | onDrop : Actor sheet is read only/locked");
            return;
        }

        const data = JSON.parse(event.dataTransfer?.getData("text/plain"));
        if (!data?.uuid) {
            console.log("Cats! | onDrop : uuid is empty");
            return;
        }

        // Item treatments
        if (data.type === "Item") {
            const item = await fromUuid(data.uuid);
            if (!item) {
                console.log("Cats! | onDrop : Item not found");
                return;
            }

            // Not usable by this character: do not block, only display a message
            if (!item.usableBy[this.actor.system.specie]) {
                ui.notifications.info(game.i18n.format("cats.items_sheet.not_usable_by", { name: item.name }));

                // Breed are only for cats
                if (item.type === 'cat_breed') {
                    return;
                }
            }

            // Dropped an item with same "id" as one owned
            if (this.actor.items) {
                // Exit if we already owned exactly this id (drag a personal item on our own sheet)
                if (this.actor.items.some((embedItem) => embedItem._id === item._id)) {
                    console.log("Cats! | This element has been ignored because it already exists in this actor", item.uuid);
                    return;
                }

                // Add quantity instead if they have (id is different so use type and name)
                if (item.system.quantity) {
                    const tmpItem = this.actor.items.find(
                        (embedItem) => embedItem.name === item.name && embedItem.type === item.type
                    );
                    if (tmpItem && this.#modifyQuantity(tmpItem.id, 1)) {
                        return;
                    }
                }
            }

            // Specific treatments
            switch (item.type) {
                case "attribute":
                    console.warn("Cats! | Attributes are only for tooltips, they cannot be drop in sheet.");
                    return;

                case "cat_breed":
                    await this.#onDropCatBreed(item);
                    break;

                case "faction":
                    await this.#onDropFaction(item);
                    break;
            }

            // Create the item on actor
            const created = await this.actor.createEmbeddedDocuments("Item", [foundry.utils.duplicate(item)]);
            if (created?.length === 1) {
                // Flag the source GUID
                const document = created[0];
                if (item.uuid && !document._stats?.compendiumSource) {
                    document.updateSource({ "_stats.compendiumSource": item.uuid });
                }
            }

            return created;
        }

        // Regular
        return super._onDrop(event);
    }

    /**
     * Alter this actor on drop of a "cat_breed" Item
     * @param   {CatsItem} catBreedItem
     * @returns {Promise<void>}
     */
    async #onDropCatBreed(catBreedItem) {
        // 1. Set cat breed identity
        await this.actor.update({ "system.identity.breed" : catBreedItem.name });

        // 2. Clear all "cat_breed" and advantages/disadvantages items
        const deleteIds = this.actor.items.filter((e) => ["cat_breed", "advantage"].includes(e.type)).map((e) => e.id);
        if (deleteIds.length > 0) {
            await this.actor.deleteEmbeddedDocuments("Item", deleteIds);
        }

        // 3. Set advantages/disadvantages
        for (const adv of catBreedItem.system.advantages) {
            const newAdv = await fromUuid(adv.uuid);
            if (newAdv) {
                const created = await this.actor.createEmbeddedDocuments("Item", [foundry.utils.duplicate(newAdv)]);
                const document = created[0];
                if (newAdv.uuid && document.uuid && !document._stats?.compendiumSource) {
                    document.updateSource({ "_stats.compendiumSource": newAdv.uuid });
                }
            }
        }
    }

    /**
     * Alter this actor on drop of a "faction" Item
     * @param   {CatsItem} factionItem
     * @returns {Promise<void>}
     */
    async #onDropFaction(factionItem) {
        // 1. Set faction identity
        await this.actor.update({ "system.identity.faction" : factionItem.name });

        // 2. Clear all "faction" items
        const deleteIds = this.actor.items.filter((e) => "faction" === e.type).map((e) => e.id);
        if (deleteIds.length > 0) {
            await this.actor.deleteEmbeddedDocuments("Item", deleteIds);
        }
    }

    /**
     * This method is called upon form submission after form data is validated
     * @param event {Event}       The initial triggering submission event
     * @param formData {Object}   The object of validated form data with which to update the object
     * @returns {Promise}         A Promise which resolves once the update operation has completed
     * @override
     */
    async _updateObject(event, formData) {
        // Make a "real" object
        formData = foundry.utils.expandObject(formData);

        // Update items ranks
        if (formData.itemsValues) {
            this.#updateItemsRank(formData.itemsValues);
        }

        return super._updateObject(event, formData);
    }

    /**
     * Update embedded items ranks
     * @param {Object<String:String>} itemsValues items new values "ids: rank"
     */
    #updateItemsRank(itemsValues) {
        Object.entries(itemsValues).forEach(([key, rank]) => {
            const item = this.actor.items.get(key);
            if (!item || item.system.rank === rank) {
                return;
            }
            item.update({ "system.rank": rank });
        });
    }

    /**
     * Add or subtract a quantity to a owned item
     * @private
     */
    #modifyQuantity(itemId, add) {
        const tmpItem = this.actor.items.get(itemId);
        if (tmpItem) {
            tmpItem.system.quantity = Math.max(1, tmpItem.system.quantity + add);
            tmpItem.update({ "system.quantity": tmpItem.system.quantity });
            return true;
        }
        return false;
    }

    /**
     * Subscribe to events from the sheet.
     * @param {jQuery} html HTML content of the sheet.
     */
    activateListeners(html) {
        super.activateListeners(html);

        // Commons
        game.cats.helper.commonListeners(html);


        // *** Everything below here is only needed if the sheet is editable ***
        if (!this.isEditable) {
            return;
        }

        // Rolls
        html.find(".ability-name").on("click", this.#openAttributeDiceRoll.bind(this));
        html.find(".skill-name").on("click", this.#openSkillDiceRoll.bind(this));
        html.find(".talent-name").on("click", this.#openTalentDialog.bind(this));

        // Add / Subtract 1 to a gauge (lives, injury, talent)
        html.find(".add-minus-control").on("mousedown", this.#modifyGauge.bind(this));

        // Soft Lock
        html.find(".soft-lock-control").on("click", this.#modifySoftLock.bind(this));


        // *** Everything below here is only allowed if the sheet is not Soft locked ***
        if (this.actor.system.soft_locked) {
            return;
        }

        // Items management
        html.find(".item-add").on("click", this.#addSubItem.bind(this));
        html.find(".item-edit").on("click", this.#editSubItem.bind(this));
        html.find(".item-delete").on("click", this.#deleteSubItem.bind(this));

        // GM Only, menu for reloading all skills
        if (game.user.isGM) {
            new ContextMenu(html, ".reload-skill-control .skills", [
                {
                    name: game.i18n.localize("cats.actor_sheet.skills.import_all"),
                    icon: '<i class="fas fa-download fa-fw"></i>',
                    callback: () => this.#importAllSkillsFromCompendiums(),
                }, {
                    name: game.i18n.localize("cats.actor_sheet.skills.clean_all"),
                    icon: '<i class="fas fa-trash fa-fw"></i>',
                    callback: () => this.#removeAllSkillsFromCompendiums(false),
                }, {
                    name: game.i18n.localize("cats.actor_sheet.skills.clean_all_empties"),
                    icon: '<i class="fas fa-trash-can fa-fw"></i>',
                    callback: () => this.#removeAllSkillsFromCompendiums(true),
                },
            ]);
        }

        // Points calculator tooltip
        game.cats.helper.popupManager(html.find(".creation-calculator-summary"), this.#creationCalculatorSummary.bind(this));
    }

    /**
     * Import all skills from skills compendium
     * @returns {Promise<void>}
     */
    async #importAllSkillsFromCompendiums() {
        const packName = CONFIG.cats.moduleName + ".core-skills";
        const skills = await game.cats.helper.loadCompendium(packName);
        if (skills.length < 1) {
            console.log(`Cats! | No items found in Pack [${packName}]`);
            return;
        }

        const skillsToAddList = [];
        const actorSkillNameList = this.actor.items
            .filter(i => i.type === 'skill')
            ?.map(i => i.name);

        skills.forEach(item => {
            if (!actorSkillNameList.includes(item.name)) {
                // Get the json data and replace the object id
                const tmpData = item.toObject();
                tmpData._id = foundry.utils.randomID();
                skillsToAddList.push(tmpData);
            }
        });

        if (skillsToAddList.length > 0) {
            await this.actor.createEmbeddedDocuments("Item", skillsToAddList);
        }
    }

    /**
     * Remove all/rankless skills
     * @param   {boolean} $bOnlyRankless
     * @returns {Promise<void>}
     */
    async #removeAllSkillsFromCompendiums($bOnlyRankless = false) {
        const deleteIds = this.actor.items
            .filter((item) => item.type === "skill" && (!$bOnlyRankless || item.system.rank < 1))
            .map(e => e._id);

        if (deleteIds.length > 0) {
            return this.actor.deleteEmbeddedDocuments("Item", deleteIds);
        }
    }

    /**
     * Soft Lock/Unlock the sheet
     * @param {Event} event
     * @private
     */
    async #modifySoftLock(event) {
        event.preventDefault();
        event.stopPropagation();

        this.actor.update({ "system.soft_locked": !this.actor.system.soft_locked });
    }

    /**
     * Add a generic item with sub type
     * @param {Event} event
     * @private
     */
    async #addSubItem(event) {
        event.preventDefault();
        event.stopPropagation();

        const baseType = $(event.currentTarget).data("item-type");
        if (!baseType) {
            return;
        }

        const created = await this.actor.createEmbeddedDocuments("Item", [{
            name: game.i18n.localize(`TYPES.Item.${baseType.toLowerCase()}`),
            type: baseType,
            img: `${CONFIG.cats.paths.assets}icons/items/${baseType}.svg`
        }]);
        if (created?.length < 1) {
            return;
        }
        const item = this.actor.items.get(created[0].id);

        // Patch for subtypes
        const subType = $(event.currentTarget).data("item-subtype");
        if (subType) {
            switch (baseType) {
                case "advantage":
                    item.system.advantage_type = subType;
                    break;

                case "favor":
                    item.system.favor_type = subType;
                    break;
            }
        }

        item.sheet.render(true);
    }

    /**
     * Edit a generic item with subtypes
     * @param {Event} event
     * @private
     */
    #editSubItem(event) {
        event.preventDefault();
        event.stopPropagation();

        const itemId = $(event.currentTarget).data("item-id");
        if (!itemId) {
            return;
        }
        const tmpItem = this.actor.items.get(itemId);
        if (!tmpItem) {
            return;
        }
        tmpItem.sheet.render(true);
    }

    /**
     * Delete a generic item with subtypes
     * @param {Event} event
     * @private
     */
    #deleteSubItem(event) {
        event.preventDefault();
        event.stopPropagation();

        const itemId = $(event.currentTarget).data("item-id");
        if (!itemId) {
            return;
        }

        const tmpItem = this.actor.items.get(itemId);
        if (!tmpItem) {
            return;
        }

        // Remove 1 qty if possible
        if (tmpItem.system.quantity > 1 && this.#modifyQuantity(tmpItem.id, -1)) {
            return;
        }

        const callback = async () => {
            return this.actor.deleteEmbeddedDocuments("Item", [itemId]);
        };

        // Holing Ctrl = without confirm
        if (event.ctrlKey) {
            return callback();
        }

        game.cats.helper.confirmDeleteDialog(
            game.i18n.format("cats.global.delete_confirm", { name: tmpItem.name }),
            callback
        );
    }

    /**
     * Add or Subtract Lives (nine_lives), injury level (injury_level), Talent level (talent_levels_used)
     * @param {Event} event
     * @private
     */
    async #modifyGauge(event) {
        event.preventDefault();
        event.stopPropagation();

        // Mouse button : "1": left(+1), "2": middle (-99=reset),  "3": right(-1)
        const type   = $(event.currentTarget).data("type")
        const middle = event.which === 2;
        const mod    = middle ? -99 : event.which === 1 ? 1 : -1;

        switch (type) {
            case "nine_lives":
                if (!middle) {
                    await this.actor.update({
                        "system.gauges.nine_lives.value" : game.cats.helper.minMax(
                            this.actor.system.gauges.nine_lives.value + mod,
                            0,
                            this.actor.system.gauges.nine_lives.max
                        ),
                    });
                }
                break;

            case "injury_level":
                await this.actor.update({
                    "system.gauges.injury_level.value" : game.cats.helper.minMax(
                        this.actor.system.gauges.injury_level.value + mod,
                        0,
                        this.actor.system.gauges.injury_level.max
                    ),
                });
                break;

            case "talent_levels":
                await this.#modifyGaugeTalent(mod);
                break;

            // case "talent_levels_plus":
            // case "talent_levels_minus":
            //     await this.#modifyGaugeTalent((type === "talent_levels_plus" ? 1 : -1));
            //     break;

            default:
                console.warn("Cats! | Unsupported type", type);
                break;
        }
    }

    /**
     * Add or subtract "mod" to "talent_levels_used"
     * @param   {number} mod
     * @returns {Promise<void>}
     */
    async #modifyGaugeTalent(mod) {
        await this.actor.update({
            "system.gauges.talent_levels_used.value": game.cats.helper.minMax(
                this.actor.system.gauges.talent_levels_used.value + mod,
                0,
                this.actor.system.gauges.talent_levels_used.max
            )
        });
    }

    /**
     * Open the dice-dialog for this attribute
     * @param {Event} event
     * @private
     */
    #openAttributeDiceRoll(event) {
        event.preventDefault();
        event.stopPropagation();

        const attributeId = $(event.currentTarget).data("id") || null;
        if (!attributeId || !this.document.system.attributes[attributeId]) {
            console.warn("Cats! | Rolling attribute but attributeId is empty or unknown");
            return;
        }

        // Open dice picker
        new game.cats.classes.CatsDicePickerDialog({
            actor: this.actor,
            attributeId: attributeId,
        }).render(true);
    }

    /**
     * Open the dice-dialog for this skill
     * @param {Event} event
     * @private
     */
    #openSkillDiceRoll(event) {
        event.preventDefault();
        event.stopPropagation();

        const skillId = $(event.currentTarget).data("id") || null;
        if (!skillId) {
            console.warn("Cats! | Rolling skill but skillId is empty");
            return;
        }
        const skill = this.actor.items.get(skillId);
        if (!skill) {
            console.warn("Cats! | Rolling skill but this skillId is unknown in items");
            return;
        }

        // Not usable by this character
        if (!skill.usableBy[this.actor.system.specie]) {
            ui.notifications.warn(game.i18n.format("cats.items_sheet.not_usable_by", { name: skill.name }));
            return;
        }

        // Omega skill need at least 1 rank to be used
        if (skill.system.omega && skill.system.rank < 1) {
            ui.notifications.warn(game.i18n.format("cats.items_sheet.omega_rank_needed", { name: skill.name }));
            return;
        }

        // Open dice picker
        new game.cats.classes.CatsDicePickerDialog({
            actor: this.actor,
            item: skill,
        }).render(true);
    }

    /**
     * Open the dialog for this talent
     * @param {Event} event
     * @private
     */
    #openTalentDialog(event) {
        event.preventDefault();
        event.stopPropagation();

        const talentId = $(event.currentTarget).data("id") || null;
        if (!talentId) {
            console.warn("Cats! | Using Talent, but talentId is empty");
            return;
        }
        const talent = this.actor.items.get(talentId);
        if (!talent) {
            console.warn(`Cats! | Using Talent, but this talentId[${talentId}] is unknown in items`);
            return;
        }

        // Not usable by this character
        if (!talent.usableBy[this.actor.system.specie]) {
            ui.notifications.warn(game.i18n.format("cats.items_sheet.not_usable_by", { name: talent.name }));
            return;
        }

        // Open talent dialog
        new game.cats.classes.CatsTalentPickerDialog({
            actor: this.actor,
            item: talent,
        }).render(true);
    }

    /**
     * Display the Creation calculator summary popup
     * @param {Event} event
     * @private
     */
    async #creationCalculatorSummary(event) {
        const system = this.actor.system;
        const specieConfig = this.actor.specieConfig;
        const progression = CONFIG.cats.progression;

        const summary = {
            isCat: this.actor.isCat,
            attributes: {
                given: specieConfig.baseCreationPoints,
                mod: 0,
                sum: 0,
                used: Object.values(system.attributes).reduce((acc, attr) => acc + attr, 0),
                logs: [],
            },
            skills: {
                given: (system.attributes.caress + system.attributes.purring) * 3,
                mod: 0,
                sum: 0,
                used: 0,
                logs: [],
                diy: [],
            },
            talents: {
                given: progression.talentGiven[system.attributes.whiskers],
                used: 0,
            }
        };

        this.actor.items.forEach((item) => {
            switch (item.type) {
                case 'advantage': {
                    // Skill count Modifiers (ex: +4, -8)
                    const modSkill = Number(item.system.modifier?.skill) || 0;
                    if (!Number.isNaN(modSkill) && modSkill !== 0) {
                        summary.skills.mod += modSkill;
                        summary.skills.logs.push(`${modSkill > 0 ? '+' : ''}${modSkill} ${item.name}`);
                    }

                    // Modifiers (ex: "stealth-1", "eye+1")
                    const modFormula = item.system.modifier?.formula?.match(/^([a-z]+)\s*([+-])\s*(\d)$/i);
                    if (modFormula?.length === 4) {
                        const target   = modFormula[1].toLowerCase();
                        const modValue = Number(`${modFormula[2]}${modFormula[3]}`);

                        if (CONFIG.cats.attributes.includes(target)) {
                            // Attribute
                            summary.attributes.mod += modValue;
                            summary.attributes.logs.push(
                                modValue
                                + ' ' + game.i18n.localize(`cats.actor_sheet.attributes.${target}`)
                                + ` (${item.name})`
                            );

                        } else {
                            // Skill
                            const searchName = game.cats.helper.normalize(target);
                            const skill = this.actor.items.find((item) =>
                                item.type === 'skill' && (
                                    game.cats.helper.normalize(item.name) === searchName
                                    || game.cats.helper.normalize(item.flags?.babele?.originalName) === searchName
                                )
                            );

                            // To do manually after all creation
                            summary.skills.diy.push(`${modValue} ${skill?.name || 'unknown'}`);
                        }
                    }
                    break;
                }
                case 'skill': {
                    if (item.system.rank > 0) {
                        // Modifiers (ex: "h+1", "c-1")
                        let rankForCost  = item.system.rank;
                        const modFormula = item.system.modifier?.match(/^([cbh])\s*([+-])\s*(\d)$/i);
                        if (modFormula?.length === 4 && modFormula[1].toLowerCase() === system.specie[0]) {
                            rankForCost = game.cats.helper.minMax(
                                item.system.rank + Number(`${modFormula[2]}${modFormula[3]}`),
                                0,
                                6
                            );
                        }
                        summary.skills.used += progression.cost[game.cats.helper.minMax(rankForCost, 0, 6)];
                    }
                    break;
                }
                case 'talent': {
                    if (item.system.rank > 0) {
                        summary.talents.used += progression.cost[game.cats.helper.minMax(item.system.rank)];
                    }
                    break;
                }
            }
        });

        // Do the sum for better readability in template
        summary.attributes.sum = summary.attributes.given + summary.attributes.mod;
        summary.skills.sum     = summary.skills.given + summary.skills.mod;

        // Render template
        const tpl = await renderTemplate(`${CONFIG.cats.paths.templates}actors/character/creation-calculator-text.html`, summary);
        if (!tpl) {
            return null;
        }
        return tpl;
    }
}
