// Import Commons Modules
import { CatsConfig } from "./config.js";
import { RegisterSettings } from "./settings.js";
import { PreloadTemplates } from "./preloadTemplates.js";
import { RegisterHandlebars } from "./handlebars.js";
import { CatsMigration } from "./migration.js";
import { CatsHooks } from "./hooks.js";
import { CatsHelpers } from "./helpers.js";
import { CatsSocketHandler } from "./socket-handler.js";
import { CatsGmDuel } from "./gm/gm-duel.js";
import { CatsGmMonitor } from "./gm/gm-monitor.js";
// Actors
import { CatsActor } from "./actor.js";
import { CatsCharacterSheet } from "./actors/character-sheet.js";
// Combat
import { CatsCombat } from "./combat.js";
// Items
import { CatsItem } from "./item.js";
import { CatsItemSheet } from "./items/item-sheet.js";
import { CatsSkillSheet } from "./items/skill-sheet.js";
import { CatsTalentSheet } from "./items/talent-sheet.js";
import { CatsAdvantageSheet } from "./items/advantage-sheet.js";
import { CatsCatBreedSheet } from "./items/cat-breed-sheet.js";
import { CatsFactionSheet } from "./items/faction-sheet.js";
import { CatsFavorSheet } from "./items/favor-sheet.js";
import { CatsAttributeSheet } from "./items/attribute-sheet.js";
// DiceRoll
import { CatsRoll } from "./dice/roll.js";
import { CatsDicePickerDialog } from "./dice/dice-picker-dialog.js";
import { CatsTalentPickerDialog } from "./dice/talent-picker-dialog.js";


/* ------------------------------------ */
/* Initialize system                    */
/* ------------------------------------ */
Hooks.once("init", async () => {
    // ***** Initializing *****
    console.log(
        "\n" +
        "   ____      _       _ \n" +
        "  / ___|__ _| |_ ___| |\n" +
        " | |   / _` | __/ __| |\n" +
        " | |__| (_| | |_\\__ \\_|\n" +
        "  \\____\\__,_|\\__|___(_)\n" +
        "                       \n" +
        "\n"
    );

    // ***** Config *****
    // Global access to Cats! Config
    CONFIG.cats = CatsConfig;

    // Assign custom classes and constants here
    CONFIG.Combat.documentClass = CatsCombat;
    CONFIG.Actor.documentClass = CatsActor;
    CONFIG.Actor.sheetClasses = CatsCharacterSheet;
    CONFIG.Item.documentClass = CatsItem;

    // Define custom Roll class
    CONFIG.Dice.rolls.unshift(CatsRoll);

    // Add some classes in game
    game.cats = {
        classes: {
            CatsActor,
            CatsRoll,
            CatsDicePickerDialog,
            CatsTalentPickerDialog,
            CatsGmDuel,
            CatsGmMonitor,
            CatsCharacterSheet,
        },
        helper: CatsHelpers,
        sockets: new CatsSocketHandler(),
        migrations: CatsMigration,
    };

    // Register custom system settings
    RegisterSettings();

    // Register custom Handlebars Helpers
    RegisterHandlebars();

    // Preload Handlebars templates (Important : Do not await ! It's sometime break the css in clients)
    PreloadTemplates().then(() => {});

    // ***** Register custom sheets *****
    // Actors
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet(CatsConfig.moduleName, CatsCharacterSheet, {
        types: ["character"],
        label: "TYPES.Actor.character",
        makeDefault: true,
    });

    // Items
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet(CatsConfig.moduleName, CatsAdvantageSheet, {
        types: ["advantage"],
        label: "TYPES.Item.advantage",
        makeDefault: true,
    });
    Items.registerSheet(CatsConfig.moduleName, CatsAttributeSheet, {
        types: ["attribute"],
        label: "TYPES.Item.attribute",
        makeDefault: true,
        canConfigure: false,
    });
    Items.registerSheet(CatsConfig.moduleName, CatsCatBreedSheet, {
        types: ["cat_breed"],
        label: "TYPES.Item.cat_breed",
        makeDefault: true,
    });
    Items.registerSheet(CatsConfig.moduleName, CatsFactionSheet, {
        types: ["faction"],
        label: "TYPES.Item.faction",
        makeDefault: true,
    });
    Items.registerSheet(CatsConfig.moduleName, CatsFavorSheet, {
        types: ["favor"],
        label: "TYPES.Item.favor",
        makeDefault: true,
    });
    Items.registerSheet(CatsConfig.moduleName, CatsItemSheet, {
        types: ["item"],
        label: "TYPES.Item.item",
        makeDefault: true,
    });
    Items.registerSheet(CatsConfig.moduleName, CatsSkillSheet, {
        types: ["skill"],
        label: "TYPES.Item.skill",
        makeDefault: true,
    });
    Items.registerSheet(CatsConfig.moduleName, CatsTalentSheet, {
        types: ["talent"],
        label: "TYPES.Item.talent",
        makeDefault: true,
    });

    // Override the default Token _drawBar function to allow bar reversing.
    Token.prototype._drawBar = function (number, bar, data) {
        const reverseBar = ["gauges.injury_level", "gauges.talent_levels_used"].includes(data.attribute);
        // && game.settings.get(CATS.moduleName, "tokenReverseBar");

        // Bar value
        const pct = Math.clamped(Number(data.value), 0, data.max) / data.max;

        // Modify color
        let color = number === 0 ? [pct / 1.2, 1 - pct, 0] : [0.5 * pct, 0.7 * pct, 0.5 + pct / 2];

        // Red if talentlvl > 24
        if (data.attribute === "gauges.talent_levels_used" && data.value > 24) {
            color = [1, 0.1, 0.1];
        }

        // Enlarge the bar for large tokens
        let h = Math.max(canvas.dimensions.size / 12, 8);
        if (this.height >= 2) {
            h *= 1.6;
        }

        // Draw the bar
        bar.clear()
            .beginFill(0x000000, 0.5)
            .lineStyle(2, 0x000000, 0.9)
            .drawRoundedRect(0, 0, this.w, h, 3)
            .beginFill(PIXI.utils.rgb2hex(color), 0.8)
            .lineStyle(1, 0x000000, 0.8)
            .drawRoundedRect(1, 1, (reverseBar ? 1 - pct : pct) * (this.w - 2), h - 2, 2);

        // Set position
        bar.position.set(0, number === 0 ? this.h - h : 0);
    };
});

/* ------------------------------------ */
/*             Others Hooks             */
/* ------------------------------------ */
Hooks.once("setup", CatsHooks.setup);
Hooks.once("ready", CatsHooks.ready);
Hooks.on("getSceneControlButtons", CatsHooks.getSceneControlButtons)