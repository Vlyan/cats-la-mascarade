export class CatsItem extends Item {
    /**
     * Create a new entity using provided input data
     * @override
     */
    static async create(data, context = {}) {
        if (data.img === undefined) {
            data.img = `${CONFIG.cats.paths.assets}icons/items/${data.type}.svg`;
        }
        return super.create(data, context);
    }

    /** @override */
    prepareData() {
        super.prepareData();

        // Sanitize some values
        switch (this.type) {
            case "skill":
                this.system.rank = game.cats.helper.minMax(this.system.rank);

                // Formula check
                this.system.formula_valid = game.cats.helper.applyFormula(this.system.formula) > 0;
                break;

            case "talent":
                this.system.rank            = game.cats.helper.minMax(this.system.rank);
                this.system.max_rank.cat    = game.cats.helper.minMax(this.system.max_rank.cat);
                this.system.max_rank.bastet = game.cats.helper.minMax(this.system.max_rank.bastet);
                this.system.max_rank.human  = game.cats.helper.minMax(this.system.max_rank.human);

                this.system.usable_by = (this.system.max_rank.cat > 0 ? 'C' : '')
                    + (this.system.max_rank.bastet > 0 ? 'B' : '')
                    + (this.system.max_rank.human > 0 ? 'H' : '');
                break;
        }
    }

    /**
     * Render the text template for this Item (tooltips and chat)
     * @return {Promise<string|null>}
     */
    async renderTextTemplate() {
        const sheetData = (await this.sheet?.getData()) || this;
        const type = this.type.replace("_", "-");
        const tpl = await renderTemplate(`${CONFIG.cats.paths.templates}items/${type}/${type}-text.html`, sheetData);
        if (!tpl) {
            return null;
        }
        return tpl;
    }

    /**
     * Return permissions for all species
     * @returns {{cat: Boolean, bastet: Boolean, human: Boolean}}
     */
    get usableBy() {
        return {
            cat: this.system.usable_by.includes('C'),
            bastet: this.system.usable_by.includes('B'),
            human: this.system.usable_by.includes('H')
        }
    }
}
