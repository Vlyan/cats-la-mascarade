/**
 * Helpers - Utility methods
 */
export class CatsHelpers {
    /**
     * Subscribe to common events from the sheet.
     * @param {jQuery} html HTML content of the sheet.
     */
    static commonListeners(html) {
        // Ability to drag n drop an actor
        html.find(".dragndrop-actor-uuid").on("dragstart", (event) => {
            const actorUuid = $(event.currentTarget).data("actor-uuid");
            if (!actorUuid) {
                return;
            }

            // Actor
            if (actorUuid.startsWith('Actor.')) {
                event.originalEvent.dataTransfer.setData(
                    "text/plain",
                    JSON.stringify({
                        type: "Actor",
                        uuid: actorUuid,
                    })
                );
            }
        });

        // Open actor sheet
        html.find(".open-sheet-from-uuid").on("click", async (event) => {
            event.preventDefault();
            event.stopPropagation();
            const uuid = $(event.currentTarget).data("uuid");
            if (!uuid) {
                return;
            }
            const doc = await fromUuid(uuid);

            // "Temporary" patch for token document (cfg dialog => actor sheet)
            if (doc instanceof TokenDocument) {
                doc?.actor?.sheet?.render(true);
                return;
            }
            doc?.sheet?.render(true);
        });

        // Embedded Items detail tooltips
        this.popupManager(html.find(".show-item-tooltip"), async (event) => {
            const itemUuid = $(event.currentTarget).data("uuid");
            if (!itemUuid) {
                return;
            }

            const item = await fromUuid(itemUuid);
            if (!item || typeof item.renderTextTemplate !== "function") {
                return;
            }
            return await item.renderTextTemplate();
        });

        // On focus on an numeric element, select all text for better experience
        html.find(".select-on-focus").on("focus", (event) => {
            event.preventDefault();
            event.stopPropagation();
            event.target.select();
        });
    }

    /**
     * Do the Popup for the selected element
     * @param {Selector} selector HTML Selector
     * @param {function} callback Callback function(event), must return the html to display
     */
    static popupManager(selector, callback) {
        const catsPopupId   = "cats-tooltip-control";
        const popupPosition = (event, popup) => {
            let left = +event.clientX + 60,
                top = +event.clientY;

            let maxY = window.innerHeight - popup.outerHeight();
            if (top > maxY) {
                top = maxY - 10;
            }

            let maxX = window.innerWidth - popup.outerWidth();
            if (left > maxX) {
                left -= popup.outerWidth() + 100;
            }
            return { left: left + "px", top: top + "px", visibility: "visible" };
        };

        selector
            .on("mouseenter", async (event) => {
                $(document.body).find(`#${catsPopupId}`).remove();
                const tpl = await callback(event);
                if (!!tpl) {
                    $(document.body).append(`<div id="${catsPopupId}" class="cats-tooltip cats-tooltip-control">${tpl}</div>`);
                }
            })
            .on("mousemove", (event) => {
                const popup = $(document.body).find(`#${catsPopupId}`);
                if (!!popup) {
                    popup.css(popupPosition(event, popup));
                }
            })
            .on("mouseleave", () => {
                $(document.body).find(`#${catsPopupId}`).remove();
            });
    }

    /**
     * Load all compendium data from his id
     *
     * @param   {String}   compendium
     * @param   {Function} filter
     * @returns {Promise<Item[]>}
     */
    static async loadCompendium(compendium, filter = item => true) {
        const pack = game.packs.get(compendium);
        const data = (await pack?.getDocuments()) ?? [];
        return data.filter(filter);
    }

    /**
     * Show a confirm dialog before a deletion
     * @param {string} content
     * @param {function} callback The callback function for confirmed action
     */
    static confirmDeleteDialog(content, callback) {
        new Dialog({
            title: game.i18n.localize("Delete"),
            content,
            buttons: {
                confirm: {
                    icon: '<i class="fas fa-trash"></i>',
                    label: game.i18n.localize("Yes"),
                    callback,
                },
                cancel: {
                    icon: '<i class="fas fa-times"></i>',
                    label: game.i18n.localize("No"),
                },
            },
        }).render(true);
    }

    /**
     * Remove default text if undefined
     * @param   {string} str
     * @returns {string}
     */
    static sanitizeLocalize(str) {
        str = game.i18n.localize(str);
        return str.indexOf("cats.") !== -1 && str.indexOf("undefined") ? "" : str;
    }

    /**
     * Sanitize a value between a range
     * @param {number} value
     * @param {number} min
     * @param {number} max
     * @returns {number}
     */
    static minMax(value, min = 0, max = 5) {
        return Math.min(max, Math.max(min, value));
    }

    /**
     * Return the computed value of a formula for this actor (or fake value if no actor attributes provided)
     * @param   {string} formula    "(tail + eye)/2"
     * @param   {object} attributes actor.system.attributes
     * @returns {number}
     */
    static applyFormula(formula, attributes = null) {
        // If not provided, fill with fake value (used to check if formula is valid)
        if (!attributes) {
            attributes = {
                "luck": 1,
                "claw": 1,
                "hair": 1,
                "eye": 1,
                "tail": 1,
                "caress": 1,
                "purring": 1,
                "pad": 1,
                "whiskers": 1
            };
        }

        // replace attr by their number
        const computedFormula = formula
            .replace(/([a-zA-Z]+)/g, (m, i) => i in attributes ? attributes[i] : m)
            .replace(/([^0-9\-+\/*()])/g, '');// arithmetics only for eval

        // Eval and round
        return Math.ceil(eval(computedFormula) ?? 0);
    }

    /**
     * Return the string simplified for comparaison
     * @param  {string|null|undefined} str
     * @return {string}
     */
    static normalize(str) {
        if (!str?.length) {
            return "";
        }
        return str
            .normalize("NFKD")
            .replace(/[\u0300-\u036f]/g, "") // remove accents
            .replace(/[\W]/g, " ") // remove non word things
            .toLowerCase()
            .trim();
    }

    /**
     * Autocomplete for an input, from array values
     * @param {jQuery}   html HTML content of the sheet.
     * @param {string}   name Html name of the input
     * @param {string[]} list Array of string to display
     * @param {string}   sep  Separator (optional, default "")
     */
    static autocomplete(html, name, list = [], sep = "") {
        const inp = document.getElementsByName(name)?.[0];
        if (!inp || list.length < 1) {
            return;
        }
        let currentFocus;

        // Add wrapper class to the parent node of the input
        inp.classList.add("autocomplete");
        inp.parentNode.classList.add("autocomplete-wrapper");

        const closeAllLists = (elmnt = null) => {
            const collection = document.getElementsByClassName("autocomplete-list");
            for (let item of collection) {
                if (!elmnt || (elmnt !== item && elmnt !== inp)) {
                    item.parentNode.removeChild(item);
                }
            }
        };

        // Abort submit on change in foundry form
        inp.addEventListener("change", (e) => {
            if (e.doSubmit) {
                closeAllLists();
                if (e.autoCompleteSelectedIndex) {
                    $(inp).prepend(
                        `<input type="hidden" name="autoCompleteListName" value="${name}">` +
                        `<input type="hidden" name="autoCompleteListSelectedIndex" value="${e.autoCompleteSelectedIndex}">`
                    );
                }
                $(inp).parent().submit();
                return true;
            }
            e.preventDefault();
            e.stopPropagation();
            return false;
        });

        // execute a function when someone writes in the text field
        inp.addEventListener("input", (inputEvent) => {
            closeAllLists();

            let val = inputEvent.target.value.trim();
            if (!val) {
                return false;
            }

            // separator
            let previous = [val];
            let currentIdx = 0;
            if (sep) {
                currentIdx = (
                    val.substring(0, inputEvent.target.selectionStart).match(new RegExp(`[${sep}]`, "g")) || []
                ).length;
                previous = val.split(sep);
                val = previous[currentIdx].trim();
            }

            currentFocus = -1;

            // create a DIV element that will contain the items (values)
            const listDiv = document.createElement("DIV");
            listDiv.setAttribute("id", inputEvent.target.id + "autocomplete-list");
            listDiv.setAttribute("class", "autocomplete-list");

            // append the DIV element as a child of the autocomplete container
            inputEvent.target.parentNode.appendChild(listDiv);

            list.forEach((value, index) => {
                if (CatsHelpers.normalize(value.substring(0, val.length)) === CatsHelpers.normalize(val)) {
                    const choiceDiv = document.createElement("DIV");
                    choiceDiv.setAttribute("data-id", index);
                    choiceDiv.innerHTML = `<strong>${value.substring(0, val.length)}</strong>${value.substring(
                        val.length
                    )}`;

                    choiceDiv.addEventListener("click", (clickEvent) => {
                        const selectedIndex = clickEvent.target.attributes["data-id"].value;
                        if (!list[selectedIndex]) {
                            return;
                        }
                        previous[currentIdx] = list[selectedIndex];
                        inp.value = previous.map((e) => e.trim()).join(sep + " ");

                        const changeEvt = new Event("change");
                        changeEvt.doSubmit = true;
                        changeEvt.autoCompleteSelectedIndex = selectedIndex;
                        inp.dispatchEvent(changeEvt);
                    });
                    listDiv.appendChild(choiceDiv);
                }
            });
        });

        // execute a function presses a key on the keyboard
        inp.addEventListener("keydown", (e) => {
            const collection = document.getElementById(e.target.id + "autocomplete-list")?.getElementsByTagName("div");
            if (!collection) {
                return;
            }
            switch (e.code) {
                case "ArrowUp":
                case "ArrowDown":
                    // focus index
                    currentFocus += e.code === "ArrowUp" ? -1 : 1;
                    if (currentFocus >= collection.length) {
                        currentFocus = 0;
                    }
                    if (currentFocus < 0) {
                        currentFocus = collection.length - 1;
                    }
                    // css classes
                    for (let item of collection) {
                        item.classList.remove("autocomplete-active");
                    }
                    collection[currentFocus]?.classList.add("autocomplete-active");
                    break;

                case "Tab":
                case "Enter":
                    e.preventDefault();
                    if (currentFocus > -1 && !!collection[currentFocus]) {
                        collection[currentFocus].click();
                    }
                    break;

                case "Escape":
                    closeAllLists();
                    break;
            } //swi
        });

        // Close all list when click in the document (1st autocomplete only)
        if (html.find(".autocomplete").length <= 1) {
            html[0].addEventListener("click", (e) => {
                const collection = document
                    .getElementById(e.target.id + "autocomplete-list")
                    ?.getElementsByTagName("div");
                if (collection !== undefined) {
                    const changeEvt = new Event("change");
                    changeEvt.doSubmit = true;
                    inp.dispatchEvent(changeEvt);
                } else {
                    closeAllLists(e.target);
                }
            });
        }
    }

    /**
     * Isolated Debounce by ID
     *
     * Usage : game.cats.helper.debounce('appId', (text) => { console.log(text) })('my text');
     *
     * @param id       Named id
     * @param callback Callback function
     * @param timeout  Wait time (500ms by default)
     * @param leading  If true the callback will be executed only at the first debounced-function call,
     *                 otherwise the callback will only be executed `delay` milliseconds after the last debounced-function call
     * @return {(function(...[*]=): void)|*}
     */
    static debounce(id, callback, timeout = 500, leading = false) {
        if (!this.debounce.timeId) {
            this.debounce.timeId = {};
        }
        return (...args) => {
            if (leading) {
                // callback will be executed only at the first debounced-function call
                if (!this.debounce.timeId[id]) {
                    callback.apply(this, args);
                }
                clearTimeout(this.debounce.timeId[id]);
                this.debounce.timeId[id] = setTimeout(() => {
                    this.debounce.timeId[id] = undefined;
                }, timeout);
            } else {
                // callback will only be executed `delay` milliseconds after the last debounced-function call
                clearTimeout(this.debounce.timeId[id]);
                this.debounce.timeId[id] = setTimeout(() => {
                    callback.apply(this, args);
                }, timeout);
            }
        };
    }

    /**
     * Send a refresh to socket, and on local window app
     * @param {String} appId Application name
     */
    static refreshLocalAndSocket(appId) {
        game.cats.sockets.refreshAppId(appId);
        Object.values(ui.windows)
            .find((e) => e.id === appId)
            ?.refresh();
    }

    /**
     * Shortcut method to draw names to chat (private) from a world table or from compendium without importing it
     *
     * Basic usage : await game.cats.helper.drawMany({tableName: "Lignées"});
     *
     * @param {String} pack                Compendium name
     * @param {String} tableName           Table name/id in this compendium
     * @param {String} retrieve            How many draw we do
     * @param {object} opt                 drawMany config option object
     * @return {Promise<{RollTableDraw}>}  The drawn results
     */
    static async drawMany({
        tableName,
        pack = null,
        retrieve = 5,
        opt = { rollMode: "selfroll" }
    }) {
        if (!pack) {
            // World Table
            const table = await (/^[a-zA-Z0-9]{16}$/.test(tableName) ? game.tables.get(tableName) : game.tables.getName(tableName));
            if (!table) {
                console.log(`Cats! | Helpers | Table not found[${tableName}]`, table);
                return;
            }
            return await table.drawMany(retrieve, opt);
        }

        // Compendiums
        const comp = await game.packs.get(pack);
        if (!comp) {
            console.log(`Cats! | Helpers | Pack not found[${pack}]`);
            return;
        }
        await comp.getDocuments();

        const table = await (/^[a-zA-Z0-9]{16}$/.test(tableName) ? comp.get(tableName) : comp.getName(tableName));
        if (!table) {
            console.log(`Cats! | Helpers | Table not found[${tableName}]`, comp, table);
            return;
        }
        return await table.drawMany({ pack: retrieve, tableName: opt });
    }

    /**
     * Return the name of the corresponding CoreCompendium for this Item
     * @param   {CatsItem} item
     * @returns {string|undefined}
     */
    static getCoreCompendiumNameByItemType(item) {
        const prefix = `${CONFIG.cats.moduleName}.core-`;
        switch (item.type) {
            case "advantage": return `${prefix}${item.system.advantage_type === "advantage" ? "advantages" : "disadvantages"}`;
            case "cat_breed": return `${prefix}cat-breeds`;
            case "faction":   return `${prefix}factions`;
            case "favor":     return `${prefix}favors`;
            case "item":      return `${prefix}items`;
            case "skill":     return `${prefix}skills`;
            case "talent":    return `${prefix}talents`;
            default:
                console.error(`Cats! | getCoreCompendiumNameByItemType | Unknown item type : ${item.type}[${item._id}]`);
                return undefined;
        }
    }
}
