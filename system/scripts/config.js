export const CatsConfig = {
    moduleName: "cats-la-mascarade",

    paths: {
        assets: `systems/cats-la-mascarade/assets/`,
        templates: `systems/cats-la-mascarade/templates/`,
    },

    // Species config
    species: {
        cat: {
            maxLives: 9,
            baseCreationPoints: 28,
            maxAttributes: {
                eye: 5,
                tail: 5,
            }
        },
        bastet: {
            maxLives: 5,
            baseCreationPoints: 26,
            maxAttributes: {
                eye: 4,
                tail: 5,
            }
        },
        human: {
            maxLives: 3,
            baseCreationPoints: 24,
            maxAttributes: {
                eye: 4,
                tail: 4,
            }
        },
    },

    // Attributes list
    attributes: [
        "claw"  , "hair"   , "eye", "tail",
        "caress", "purring", "pad", "whiskers",
        "luck"
    ],

    difficulty: {
        levels: [1, 3, 5, 7, 9, 11, 13, 15, 17, 19],
    },

    // Character progression/creation stuff
    progression: {
        // Whiskers 1-5 (+ upgrades)
        talentGiven: [0, 2, 4, 8, 16, 24, 48, 96],

        // Skill/Talent cost by rank 0-5 (+ modifiers)
        cost: [0, 1, 2, 4, 8, 16, 32, 64],
    },
};
