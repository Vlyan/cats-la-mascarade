export class CatsHooks {
    /**
     * Do anything after initialization but before ready
     */
    static setup() {
        // Enable embed Babele compendiums only if custom compendium is not found or disabled
        if (
            game.babele &&
            game.babele.modules.every((module) => module.module !== "cats-la-mascarade-custom-compendiums")
        ) {
            game.babele.setSystemTranslationsDir("babele"); // Since Babele v2.0.7
        }
    }

    /**
     * Do anything once the system is ready
     */
    static async ready() {
        // If multiple GM connected, tag the 1st alive, useful for some traitements that need to be done once (migration, delete...)
        Object.defineProperty(game.user, "isFirstGM", {
            get: function() {
                return game.user.isGM && game.user.id === game.users.find((u) => u.active && u.isGM)?.id;
            }
        });

        // Migration stuff
        if (game.user.isFirstGM && game.cats.migrations.needUpdate(game.cats.migrations.NEEDED_VERSION)) {
            game.cats.migrations.migrateWorld({ force: false }).then();
        }

        // For some reasons, not always really ready, so wait a little
        await new Promise((r) => setTimeout(r, 2000));


        // Show GM Monitor on start
        if (game.user.isGM && game.settings.get(CONFIG.cats.moduleName, "showGmMonitorOnStartup")) {
            new game.cats.classes.CatsGmMonitor().render(true);
        }

        // ***** UI *****
        // Open a funny cat on logo clic
        $("#logo")
            .on("click", async () => {
                const sc = $(document.body).find("#swinging-cat-wrapper");
                if (sc.length > 0) {
                    sc.remove();

                } else {
                    const tpl = await renderTemplate(`${CONFIG.cats.paths.templates}ui/swinging-cat.html`, {});
                    $(document.body).append(`<div id="swinging-cat-wrapper" class="cats">${tpl}</div>`);
                }
            })
            .prop("title", game.i18n.localize("cats.global.logo"));

        // If any disclaimer show it ont start
        const disclaimer = game.i18n.localize("cats.global.disclaimer");
        if (disclaimer !== "" && disclaimer !== "cats.global.disclaimer") {
            ui.notifications.info(disclaimer);
        }
    }

    /**
     * Add GM Monitor button
     * @param   {SceneControl[]} controls
     * @returns {Promise<void>}
     */
    static async getSceneControlButtons(controls) {
        if (!game.user.isGM) {
            return;
        }

        // GM Monitor Button
        const tokenControls = controls.find(x => x.name === 'token');
        tokenControls.tools.push({
            name: "cats-gm-monitor",
            title: "cats.gm_monitor.title",
            icon: "fa-solid fa-cat",
            visible: true,
            button: true,
            toggle: false,
            onClick: () => (new game.cats.classes.CatsGmMonitor()).render(true),
        });
        tokenControls.tools.push({
            name: "cats-gm-duel",
            title: "cats.gm_duel.title",
            icon: "fa-solid fa-shield-cat",
            visible: true,
            button: true,
            toggle: false,
            onClick: () => (new game.cats.classes.CatsGmDuel()).render(true),
        });
    }
}
