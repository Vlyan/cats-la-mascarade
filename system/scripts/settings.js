/**
 * Custom system settings register
 */
export const RegisterSettings = function () {
    /* ------------------------------------ */
    /* User settings                        */
    /* ------------------------------------ */
    game.settings.register(CONFIG.cats.moduleName, "removeUnusableSkills", {
        name: "SETTINGS.removeUnusableSkills.text",
        hint: "SETTINGS.removeUnusableSkills.hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
    });

    game.settings.register(CONFIG.cats.moduleName, "chosenRuleInjuriesCritics", {
        name: "SETTINGS.chosenRuleInjuriesCritics.text",
        hint: "SETTINGS.chosenRuleInjuriesCritics.hint",
        scope: "world",
        config: true,
        default: "book",
        choices: {
            "book": "SETTINGS.chosenRuleInjuriesCritics.book",
            "keep10": "SETTINGS.chosenRuleInjuriesCritics.keep10",
            "fixed": "SETTINGS.chosenRuleInjuriesCritics.fixed"
        },
        type: String,
    });

    game.settings.register(CONFIG.cats.moduleName, "showGmMonitorOnStartup", {
        name: "SETTINGS.showGmMonitorOnStartup.text",
        hint: "SETTINGS.showGmMonitorOnStartup.hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
    });


    /* ------------------------------------ */
    /* Update                               */
    /* ------------------------------------ */
    game.settings.register(CONFIG.cats.moduleName, "systemMigrationVersion", {
        name: "System Migration Version",
        scope: "world",
        config: false,
        type: String,
        default: 0,
    });


    /* ------------------------------------ */
    /* GM Monitor windows (GM only)         */
    /* ------------------------------------ */
    game.settings.register(CONFIG.cats.moduleName, "gmMonitorActorsUuids", {
        name: "Gm Monitor",
        scope: "world",
        config: false,
        type: Array,
        default: [],
        onChange: () => game.cats.helper.refreshLocalAndSocket("cats-gm-monitor"),
    });
};
