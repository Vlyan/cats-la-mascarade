/**
 * Extends the actor to process special things from Cats!
 */
export class CatsActor extends Actor {
    /**
     * Create a new entity using provided input data
     * @override
     */
    static async create(docData, options = {}) {
        // Replace default image
        if (docData.img === undefined) {
            docData.img = `${CONFIG.cats.paths.assets}icons/actors/character.svg`;
        }

        // Load some things from compendiums (New actor has no items, duplicates does)
        if (!docData.items) {
            docData.items = [];
            await CatsActor.addSkillsFromCompendiums(docData);
            await CatsActor.addItemsFromCompendiums(docData);

            // Set some properties
            docData.prototypeToken = docData.prototypeToken || {};
            foundry.utils.mergeObject(
                docData.prototypeToken,
                {
                    // vision: true,
                    // dimSight: 30,
                    // brightSight: 0,
                    actorLink: true,
                    disposition: 0, // -1: Hostile, 0: neutral, 1: friendly
                    bar1: {
                        attribute: "gauges.injury_level",
                    },
                    bar2: {
                        attribute: "gauges.talent_levels_used",
                    },
                },
                { overwrite: false }
            );

        // } else {
            // TODO Replace the object id for duplicated objects ?
            // console.log('Cats! | Replace the items ids to duplicated Actor');
        }

        return super.create(docData, options);
    }

    /**
     * Add all the skills from compendiums to "items"
     */
    static async addSkillsFromCompendiums(docData) {
        console.log(`Cats! | Adding skills to ${docData.name}`);

        const packName = CONFIG.cats.moduleName + ".core-skills";
        const skills = await game.cats.helper.loadCompendium(packName);
        if (skills.length < 1) {
            console.log(`Cats! | No items found in Pack [${packName}]`);
            return;
        }

        // Get the json data and replace the object id
        skills.forEach(item => {
            const tmpData = item.toObject();
            tmpData._id = foundry.utils.randomID();
            docData.items.push(tmpData);
        });
    }

    /**
     * Add "Fists, Cat claws" item from compendiums to "items"
     */
    static async addItemsFromCompendiums(docData) {
        console.log(`Cats! | Adding items to ${docData.name}`);

        const weapon = await fromUuid(`Compendium.${CONFIG.cats.moduleName}.core-items.CatCoreObj000001`);
        if (!weapon) {
            return;
        }

        // Get the json data and replace the object id
        const tmpData = weapon.toObject();
        tmpData._id = foundry.utils.randomID();
        docData.items.push(tmpData);
    }

    /** @override */
    prepareData() {
        super.prepareData();

        const specieConfig = this.specieConfig;
        const gauges = this.system.gauges;

        // Sanitize Attributes
        CONFIG.cats.attributes.forEach(attr => {
            this.system.attributes[attr] = game.cats.helper.minMax(
                this.system.attributes[attr],
                1,
                specieConfig.maxAttributes[attr] ?? 5
            )
        });

        // Sanitize Gauges
        gauges.nine_lives = {
            value: game.cats.helper.minMax(gauges.nine_lives.value, 0, specieConfig.maxLives),
            max: specieConfig.maxLives,
        };
        gauges.injury_level = {
            value: game.cats.helper.minMax(gauges.injury_level.value, 0, this.getInjuryLevelMaxValue()),
            max: this.getInjuryLevelMaxValue(),
        };
        gauges.talent_levels_used = {
            value: game.cats.helper.minMax(gauges.talent_levels_used.value, 0, 40),
            max: 40,
        };

        // Modify Skill's "Base Rank"
        this.computeSkillsFromAttributes();
    }

    /**
     * Compute and set Base rank to skills depending on the actor attributes
     */
    computeSkillsFromAttributes() {
        this.items.forEach((item) => {
            if (item.type === "skill" && !!item.system.formula) {
                const base = game.cats.helper.applyFormula(item.system.formula, this.system.attributes);
                if (base > 0) {
                    item.system.base = game.cats.helper.minMax(base, 1, 5);
                }
            }
        });
    }

    /**
     * Entity-specific actions that should occur when the Entity is updated
     * @override
     */
    async update(docData = {}, context = {}) {
        // Fix foundry v0.8.8 (config token=object, update=flat array)
        docData = foundry.utils.flattenObject(docData);

        // Need a _id
        if (!docData["_id"]) {
            docData["_id"] = this.id;
        }

        // Context informations (needed for unlinked token update)
        context.parent = this.parent;
        context.pack = this.pack;

        // Only on linked Actor
        if (
            !!docData["prototypeToken.actorLink"] ||
            (docData["prototypeToken.actorLink"] === undefined && this.prototypeToken?.actorLink)
        ) {
            // Update the token name/image if the sheet name/image changed, but only if
            // they was previously the same, and token img was not set in same time
            Object.entries({ name: "name", img: "texture.src" }).forEach(([dataProp, TknProp]) => {
                if (
                    docData[dataProp] &&
                    !docData["prototypeToken." + TknProp] &&
                    this[dataProp] === foundry.utils.getProperty(this.prototypeToken, TknProp) &&
                    this[dataProp] !== docData[dataProp]
                ) {
                    docData["prototypeToken." + TknProp] = docData[dataProp];
                }
            });
        }

        // Save
        const previousSystem = foundry.utils.duplicate(this.system);
        return Actor.updateDocuments([docData], context).then(() => {
            // Injury updated ?
            if (this.system.gauges.injury_level.value !== previousSystem.gauges.injury_level.value) {
                this.applyInjuryEffect();
            }

            // Talent make injuries at some point
            this.#applyTalentLevelToInjuryLevel(
                previousSystem.gauges.talent_levels_used.value,
                this.system.gauges.talent_levels_used.value,
            );

            // Notify the "Gm Monitor" if this actor is watched
            if (game.settings.get(CONFIG.cats.moduleName, "gmMonitorActorsUuids").some((uuid) => uuid === this.uuid)) {
                game.cats.helper.refreshLocalAndSocket("cats-gm-monitor");
            }
        });
    }

    /**
     * Render the text template for this Actor (tooltips and chat)
     * @return {Promise<string|null>}
     */
    async renderTextTemplate() {
        const sheetData = (await this.sheet?.getData()) || this;
        const tpl = await renderTemplate(`${CONFIG.cats.paths.templates}actors/actor-text.html`, sheetData);
        if (!tpl) {
            return null;
        }
        return tpl;
    }

    /**
     * Return the specie config object for this actor
     * @returns {Object}
     */
    get specieConfig() {
        return CONFIG.cats.species[this.system.specie];
    }

    /**
     * Return true if this actor have an active player as owner
     * @returns {boolean}
     */
    get hasPlayerOwnerActive() {
        return game.users.find((u) => !!u.active && u.character?.id === this.id);
    }


    // *** Injuries management ***
    /**
     * Return the max injury level for this actor
     * @returns {Number}
     */
    getInjuryLevelMaxValue() {
        return this.system.attributes.hair
            // status (unconsciousness + coma + death)
            + 3
            // Humans and Bastets have an +5 to theirs hair value
            + (this.isCat ? 0 : 5);
    }

    get isUnconscious() {
        return this.system.gauges.injury_level.value === (this.getInjuryLevelMaxValue() - 2);
    }

    get isInComa() {
        return this.system.gauges.injury_level.value === (this.getInjuryLevelMaxValue() - 1);
    }

    get isDead() {
        return this.system.gauges.injury_level.value === this.getInjuryLevelMaxValue();
    }

    get isCat() {
        return this.system.specie === "cat";
    }

    get isBastet() {
        return this.system.specie === "bastet";
    }

    get isHuman() {
        return this.system.specie === "human";
    }

    /**
     * Return the injury level modifier for this actor (0, -1, ... ,-5)
     * @returns {Number}
     */
    get injuryLevelRollModifier() {
        const modifier = this.isCat ?
            this.system.gauges.injury_level.value :
            Math.ceil(this.system.gauges.injury_level.value / 2)
        ;
        const max = this.isCat ?
            this.system.attributes.hair :
            Math.ceil((this.system.attributes.hair + 5) / 2)
        ;

        return -game.cats.helper.minMax(modifier, 0, max);
    }

    /**
     * Apply status effects based on current "injury_level" value
     */
    async applyInjuryEffect() {
        // Remove all 3 effects
        await this.removeEffects(["unconscious", "paralysis", "dead"]);

        const level = this.system.gauges.injury_level.value;
        const levelMax = this.system.gauges.injury_level.max;

        if (level === (levelMax - 2)) {
            await this.addEffect("unconscious");
            await this.rollAndDisplayUnconsciousnessState();

        } else if (level === (levelMax - 1)) {
            await this.addEffect("paralysis", "cats.actor_sheet.injury_levels.coma");
            await this.displayComaState();

        } else if (level === levelMax) {
            await this.addEffect("dead");
        }
    }

    /**
     * Roll (3d10 x 5 min) to known how many minutes the actor is unconscious, and display it in the chat
     * @returns {Promise<void>}
     */
    async rollAndDisplayUnconsciousnessState() {
        // Unconsciousness : Duration = 3d10 x 5 min
        const roll = await new Roll('3d10');
        await roll.roll();

        // Send to chat
        await ChatMessage.create({
            flavor: game.i18n.format("cats.dice.unconsciousness_message", { duration: roll.total * 5 }),
            user: game.user.id,
            rolls: [roll.toJSON()],
            rollMode: CONST.DICE_ROLL_MODES.BLIND, // GM only
            sound: CONFIG.sounds.notification,
            speaker: {
                actor: this.id || null,
                token: this.token || null,
                alias: this.name || null,
            },
        });
    }

    /**
     * Display ChatMessage to help the GM on Coma state
     * @returns {Promise<void>}
     */
    async displayComaState() {
        // Send to chat
        await ChatMessage.create({
            content: game.i18n.localize("cats.dice.coma_message"),
            user: game.user.id,
            type: CONST.CHAT_MESSAGE_STYLES.IC,
            whisper: ChatMessage.getWhisperRecipients("GM").map(u => u.id), // GM only
            sound: CONFIG.sounds.notification,
            speaker: {
                actor: this.id || null,
                token: this.token || null,
                alias: this.name || null,
            },
        });
    }

    /**
     * Add this effect ID from this actor (if not already present)
     * @param {string} id    Id, see CONFIG.statusEffects
     * @param {string} label Label to override the base one
     * @returns {Promise<abstract.Document[]|void>}
     */
    async addEffect(id, label = null) {
        // Do not add the same effect twice
        if (this.statuses.has(id)) {
            console.log(`Cats! | Ignored : Effect already in actor[${id}]`);
            return;
        }

        const data = foundry.utils.duplicate(CONFIG.statusEffects.find(e => e.id === id));
        if (data) {
            return this.createEmbeddedDocuments("ActiveEffect", [{
                ...data,
                name: game.i18n.localize(label ?? data.name),
                statuses: [id],
            }]);
        }
    }

    /**
     * Remove theses effects IDs from this actor
     * @param   {string[]} ids
     * @returns {Promise<abstract.Document[]|void>}
     */
    async removeEffects(ids) {
        if (!Array.isArray(ids)) {
            ids = [ids];
        }

        const deleteIds = this.effects.filter((e) => e.statuses.some(s => ids.includes(s))).map((e) => e.id);
        if (deleteIds.length > 0) {
            return this.deleteEmbeddedDocuments("ActiveEffect", deleteIds);
        }
    }


    // *** Talent levels management ***
    /**
     * Return the talent modifier value for this actor
     * @returns {number|null}
     */
    talentModifier() {
        return CatsActor.getTalentModifier(this.system.gauges.talent_levels_used.value);
    }

    /**
     * Return the talent modifier value for this level
     * @param   {number} level 0-40
     * @returns {null|number}
     */
    static getTalentModifier(level) {
        if (level < 0 || level > 40) {
            return null;
        }
        if (level <= 24) {
            return 0;
        }
        if (level <= 32) {
            return -1;
        }
        if (level <= 36) {
            return -2;
        }
        if (level <= 38) {
            return -3;
        }
        if (level === 39) {
            return -4;
        }
        if (level === 40) {
            return -5;
        }
    }

    /**
     * Return the talent modifier difference between theses two values
     * @param   {number} prevValue Previous talent_levels
     * @param   {number} newValue  New talent_levels
     * @returns {number}
     */
    static getTalentModifierDiff(prevValue, newValue) {
        // Apply "talent_levels" modifier (0, -1, ..., -5) in "injury_level"
        const prevMod = CatsActor.getTalentModifier(prevValue);
        const newMod  = CatsActor.getTalentModifier(newValue);

        // Ignore null and same value, meaning no change
        if (newMod === null || prevMod === newMod) {
            return 0;
        }

        // Return the modifier value
        return Math.abs(newMod) - Math.abs(prevMod);
    }

    /**
     * Modify the Injury Level base on the Talent Modifier Level
     * @param   {number} prevValue Previous talent_levels
     * @param   {number} newValue  New talent_levels
     * @returns {Promise<void>}
     */
    async #applyTalentLevelToInjuryLevel(prevValue, newValue) {
        const diff = CatsActor.getTalentModifierDiff(prevValue, newValue);
        if (diff === 0) {
            return;
        }
        await this.update({
            "system.gauges.injury_level.value" : game.cats.helper.minMax(
                this.system.gauges.injury_level.value + diff,
                0,
                this.system.gauges.injury_level.max
            ),
        });
    }
}
