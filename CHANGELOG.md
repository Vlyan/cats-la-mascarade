# Changelog
> Date format : day/month/year

> Version format : `<major>.<foundry-version>.<system-version>`
> - `major`: Big breaking changes.
> - `foundry-version`: Stick to the major version of FoundryVTT.
> - `system-version`: System functionalities and Fixes.

## 1.12.0 - 19/07/2024 - Creation Calculator Update
- Changed the way of versioning the system: from `<major>.<minor>.<patch>` to `<major>.<foundry-version>.<system-version>`.
- Initial attributes are now set to `3` by default for new actors, to reduce the workload for the GM when creating NPCs.
- Added Attributes Items for Tooltips and compendium helper (not used in sheet).
- Updated the creation calculator, some items have now modifiers attributes. Some need manual attention. A migration script will trigger in this update.
  - Skills `Modifier`:
    - Format:
      - 1st character: `C`at, `B`astet or `H`uman.
      - 2nd character: `-` or `+`.
      - 3rd character: `1` to `9` (but currently only -2 to +2 are relevant).
    - Exemples (used in core book):
      - `C-1`: Used for `Jump`, shift down the cost by 1 step.
      - `H+1`: Used for `Martial Arts`, shift up the cost by 1 step.
  - Advantages:
    - `Skill Modifier`: Used for Skill point computation.
        - Exemples (used in core book):
          - `-8`: Used in `Creditor`.
          - `+4`: Used in `Hydrophile`.
    - `Modifier` format: Any Attribute or Skill name with numeric modifier prefixed by `-` or `+`.
      - Exemples (used in core book):
        - `eye+1` (attribute): Used in `Ambidextrous`.
        - `stealth-1` (skill): Used in `Sighted`.

## 1.11.0 - 31/05/2024 - FoundryVTT v12 Compatibility
__! Be certain to carefully back up any critical user data before installing this update !__
- Updated the System to FoundryVTT v12.
- Added GM Duel tool to quicken Duel for GM.
- Added `userBonus` and `userMalus` options to the dice picker constructor to set default Bonus and Malus.
- Added `openDicePicker` to sockets to allow GM to send setup dice windows (only used in GM Duel for now).
- Added icon change on advantage/disadvantage selection.

## 1.10.0 - 02/02/2024 - Happy New Cat !
- Added basic calculator popup for character making.
- Added CatBreed and Faction popup in Gm Monitor.
- System now restricted to Foundry v11+.

## 1.9.0 - 17/01/2024 - Per Favor !
- Added favor as items for an easier usage/management.
- Added favor compendium based on "favor cards" in base helpers.
- Added a warning message when using middle mouse button on reputation/lives in GM Monitor.
- Faction and Breed are now stored as item on drop on actor sheet.
- Fix for GM Monitor who do not refresh for unlinked actors.

## 1.8.0 - 20/12/2023 - Holiday's QoL
- Added ChatMessage for Coma/Unconsciousness states (GM only, but can revealed afterward).
- Using the new packFolders in system manifest.
- Fix for GM ContextMenu display in Skill row.
- Fix for Effect name (label -> name).

## 1.7.1 - 11/08/2023 - Compendium Fix
- Updated `Streetwise` formula `purring` -> `(purring + pad)/2`.

## 1.7.0 - 29/05/2023 - FoundryVTT v11 Compatibility
__! Be certain to carefully back up any critical user data before installing this update !__
- Updated the System to FoundryVTT v11.
- Added the ability to add and subtract in the same object when dropping the same item on character sheet.

## 1.6.0 - 29/01/2023 - Hybrid rule and fixes
- Added a Hybrid rule for critics in settings (the previous value cannot be migrated, please reset the option if you have this option check before).
- Added conditional visibility on difficulty display in chat, to be hidden in chat basic roll (/r 1d100).
- Fix for difficulty display in chat (9 = "Medium" not "Fairly easy")

## 1.5.0 - 01/01/2023 - Ours cats have talent !
- Added talent dialog to show in chat the usage of talent, and it's rank used.
- Added GM context menu to remove all/rankless skills on actor sheet (right clic on skill title to see the option).

## 1.4.0 - 16/12/2022 - Cats Eyes !
- Added GM Monitor window (GMs only).
  - Added an Icon in token tools to display it.
  - Added an option to display it on startup (enabled by default).
- Fixed injuries icons on chat (ex: a cat with `5` in `hair` with `6` injuries now display the coma icon instead of `6`).

## 1.3.1 - 15/12/2022 - Critics Fix
- Fixed the `luck` value that won't update on critics.

## 1.3.0 - 07/12/2022 - Actor sheet grooming
- Added `+4` (Bastet) / `+5` (Human) label on actor sheet on `Hair` and `Claw` attributes.
- Added soft-lock capability on actor sheet.
- Added formula check indicator on actor and skill sheets : the `Base value` now display a warning if `formula` is not set or invalid.
- Added GM ability to import all skills from compendiums on actor sheet (right clic on skill title to see the option).

## 1.2.0 - 03/12/2022 - Tweaks
- Added total column on skills.
- Added custom modifiers on DicePicker (+- 0 to 5).
- Added injury level on display for chat roll.
- Added skill's formula translation on the fly.
- Fix: Critical failure are only on 1 on 2nd dice (before was "1-6" if you had 5 injuries).
- Fix: Altering token bar values now applies effects.
- Fix: Reordered attributes to match rule book.
- Updated some language's typo (thanks to `Dame du Lac`).

## 1.1.1 - 01/12/2022 - CSS Fixes for text editor
- Fixed the height of editors when editing (wrong size).

## 1.1.0 - 01/12/2022 - DicePicker Dialog
- Added a dialog when rolling dice from sheets.
- Added an option to disable injuries modifier on critics rolls (1/10, 1-2/, 1-3/... → 1/10).
- Updated chat log for rolls, now display critics if any.
- Some minors fixes

## 1.0.0 - 24/11/2022 - First public release
- Character, Skill, Talent, Advantage/Disadvantage, Object/Weapons, CatBreed and Faction sheets.
- Basic roll system (no critical mechanics).
